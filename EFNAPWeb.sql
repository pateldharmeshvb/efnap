
GO
/****** Object:  Table [dbo].[tblMenuItem]    Script Date: 23-Apr-17 5:51:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMenuItem](
	[MenuItemID] [int] IDENTITY(1,1) NOT NULL,
	[MenuItem] [varchar](50) NULL,
	[MenuItemController] [varchar](50) NULL,
	[MenuItemView] [varchar](50) NULL,
	[SortOrder] [int] NULL,
	[ParentID] [int] NULL CONSTRAINT [DF__tblMenuIt__Paren__1B0907CE]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF__tblMenuIt__IsAct__1BFD2C07]  DEFAULT ((1)),
	[IsCompanyAdminType] [bit] NULL,
 CONSTRAINT [PK__tblMenuI__8943F70203317E3D] PRIMARY KEY CLUSTERED 
(
	[MenuItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblRole]    Script Date: 23-Apr-17 5:51:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRole](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NULL,
	[Description] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF__tblRole__Created__5FB337D6]  DEFAULT (getdate()),
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF__tblRole__Modifie__60A75C0F]  DEFAULT (getdate()),
	[IsActive] [bit] NULL CONSTRAINT [DF__tblRole__IsActiv__619B8048]  DEFAULT ((1)),
	[IsAdminRole] [bit] NOT NULL DEFAULT ((0)),
	[CompanyID] [int] NOT NULL,
 CONSTRAINT [PK__tblRole__8AFACE3A4222D4EF] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblRolePrivileges]    Script Date: 23-Apr-17 5:51:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRolePrivileges](
	[PrivilegeID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[MenuItemID] [int] NULL,
	[View] [bit] NULL,
	[Add] [bit] NULL,
	[Edit] [bit] NULL,
	[Delete] [bit] NULL,
	[Detail] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF__tblRolePr__Creat__628FA481]  DEFAULT (getdate()),
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF__tblRolePr__Modif__6383C8BA]  DEFAULT (getdate()),
	[CompanyID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_tblRolePrivileges_IsActive]  DEFAULT ((1)),
 CONSTRAINT [PK__tblRoleP__B3E77E3C3E52440B] PRIMARY KEY CLUSTERED 
(
	[PrivilegeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 23-Apr-17 5:51:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUser](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](250) NULL,
	[Username] [varchar](20) NULL,
	[Password] [varchar](200) NULL,
	[DateOfBirth] [datetime] NULL,
	[CountryID] [int] NULL,
	[StateID] [int] NULL,
	[CityID] [int] NULL,
	[RoleID] [int] NOT NULL,
	[IsAdminUser] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vwRolePrivileges]    Script Date: 23-Apr-17 5:51:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vwRolePrivileges]
AS
SELECT     dbo.tblMenuItem.MenuItemID, dbo.tblMenuItem.MenuItem, dbo.tblMenuItem.MenuItemController, dbo.tblMenuItem.MenuItemView, 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[View] END AS [View], 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[Add] END AS [Add], 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.Edit END AS Edit, 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[Delete] END AS [Delete], 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[Detail] END AS [Detail], 
                      dbo.tblMenuItem.SortOrder AS OrderID, dbo.tblMenuItem.ParentID, dbo.tblMenuItem.SortOrder,Convert(int, dbo.tblMenuItem.IsActive) as IsActive, 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN 0 ELSE dbo.tblRolePrivileges.RoleID END AS RoleID,
					  dbo.tblMenuItem.IsCompanyAdminType
FROM         dbo.tblMenuItem LEFT JOIN
                      dbo.tblRolePrivileges ON tblMenuItem.MenuItemID = dbo.tblRolePrivileges.MenuItemID
WHERE     dbo.tblMenuItem.ParentID = 0
UNION ALL
SELECT     dbo.tblMenuItem.MenuItemID, dbo.tblMenuItem.MenuItem, dbo.tblMenuItem.MenuItemController, dbo.tblMenuItem.MenuItemView, 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[View] END AS [View], 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[Add] END AS [Add], 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.Edit END AS Edit, 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[Delete] END AS [Delete], 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN CONVERT(bit, 0) ELSE dbo.tblRolePrivileges.[Detail] END AS [Detail],
                          (SELECT     SortOrder
                            FROM          tblMenuItem
                            WHERE      MenuItemID = dbo.tblMenuItem.ParentID) AS OrderID, dbo.tblMenuItem.ParentID, dbo.tblMenuItem.SortOrder, Convert(int, dbo.tblMenuItem.IsActive) as IsActive, 
                      CASE WHEN dbo.tblRolePrivileges.MenuItemID IS NULL THEN 0 ELSE dbo.tblRolePrivileges.RoleID END AS RoleID,
					  dbo.tblMenuItem.IsCompanyAdminType
FROM         dbo.tblMenuItem LEFT JOIN
                      dbo.tblRolePrivileges ON tblMenuItem.MenuItemID = dbo.tblRolePrivileges.MenuItemID
WHERE     dbo.tblMenuItem.ParentID > 0









GO
SET IDENTITY_INSERT [dbo].[tblMenuItem] ON 

GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (1, N'Transaction', NULL, NULL, 1, 0, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (2, N'Client', N'Customer', N'Index', 1, 1, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (3, N'Masters', NULL, NULL, 2, 0, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (4, N'User Management', NULL, NULL, 1, 3, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (5, N'User', N'User', N'Index', 1, 4, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (6, N'Role', N'Role', N'Index', 2, 4, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (7, N'Role Privileges', N'RolePrivileges', N'Index', 3, 4, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (22, N'Enum', N'Enum', N'Index', 2, 3, 1, 0)
GO
INSERT [dbo].[tblMenuItem] ([MenuItemID], [MenuItem], [MenuItemController], [MenuItemView], [SortOrder], [ParentID], [IsActive], [IsCompanyAdminType]) VALUES (23, N'Transporter', N'Transporter', N'Index', 3, 3, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[tblMenuItem] OFF
GO
SET IDENTITY_INSERT [dbo].[tblRole] ON 

GO
INSERT [dbo].[tblRole] ([RoleID], [Role], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsActive], [IsAdminRole], [CompanyID]) VALUES (1, N'Admin', NULL, NULL, NULL, NULL, NULL, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[tblRole] OFF
GO
SET IDENTITY_INSERT [dbo].[tblRolePrivileges] ON 

GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (373, 1, 1, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 03:55:49.713' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (374, 1, 2, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 03:56:16.727' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (375, 1, 3, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 03:56:32.920' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (376, 1, 4, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 03:56:56.690' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (377, 1, 5, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 03:58:30.670' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (378, 1, 6, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 04:00:19.490' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (379, 1, 7, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 04:01:49.290' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (1704, 1, 22, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 00:00:00.000' AS DateTime), 1, 1)
GO
INSERT [dbo].[tblRolePrivileges] ([PrivilegeID], [RoleID], [MenuItemID], [View], [Add], [Edit], [Delete], [Detail], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CompanyID], [IsActive]) VALUES (1705, 1, 23, 1, 1, 1, 1, 1, 1, CAST(N'2015-12-24 00:00:00.000' AS DateTime), NULL, CAST(N'2015-12-24 00:00:00.000' AS DateTime), 1, 1)
GO
SET IDENTITY_INSERT [dbo].[tblRolePrivileges] OFF
GO
SET IDENTITY_INSERT [dbo].[tblUser] ON 

GO
INSERT [dbo].[tblUser] ([UserID], [FirstName], [LastName], [Phone], [Email], [Username], [Password], [DateOfBirth], [CountryID], [StateID], [CityID], [RoleID], [IsAdminUser], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Administrator', N'User', N'7777777', N'admin@gmail.com', N'admin', N'XA/3CJlPKDnegrun63K89mBNtv+5fbGV6hqlbmafSdo=', CAST(N'1987-01-01 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, 1, 1, 1, CAST(N'2017-04-23 00:00:00.000' AS DateTime), 1, CAST(N'2017-04-23 00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tblUser] OFF
GO
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblRole] FOREIGN KEY([RoleID])
REFERENCES [dbo].[tblRole] ([RoleID])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblRole]
GO
/****** Object:  StoredProcedure [dbo].[RolePrev]    Script Date: 23-Apr-17 5:51:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- exec RolePrev 1
CREATE PROCEDURE [dbo].[RolePrev] 
	-- Add the parameters for the stored procedure here
	@RoleID int,
	@Type bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #Temp1 (MenuItemID INT,MenuItem VARCHAR(50), MenuItemController VARCHAR(50), MenuItemView VARCHAR(50), [View] BIT, [Add] BIT, [Edit] BIT, [Delete] BIT,[Detail] BIT, OrderID int,MainParentID INT,ParentID INT, SortOrder int,IsActive bit ,RoleID
 int)
	DECLARE @MenuItemID INT
	DECLARE @ParentID INT
	DECLARE @MenuItem VARCHAR(50)
	DECLARE @MenuItemController VARCHAR(50)
	DECLARE @MenuItemView VARCHAR(50)	
	DECLARE @View BIT
	DECLARE @Add BIT
	DECLARE @Edit BIT
	DECLARE @Delete BIT
	DECLARE @Detail BIT
	DECLARE @SortOrder int
	DECLARE @IsActive BIT	
	DECLARE @OrderID BIT
	
	
	DECLARE @FirstMenuItemID INT
	DECLARE @FirstParentID INT
	DECLARE @FirstMenuItem VARCHAR(50)
	DECLARE @FirstMenuItemController VARCHAR(50)
	DECLARE @FirstMenuItemView VARCHAR(50)	
	DECLARE @FirstView BIT
	DECLARE @FirstAdd BIT
	DECLARE @FirstEdit BIT
	DECLARE @FirstDelete BIT
	DECLARE @FirstDetail BIT
	DECLARE @FirstDispOrder int	
	DECLARE @FirstOrderID BIT
	DECLARE @FirstRoleID BIT
	DECLARE @FirstIsActive BIT
	
	DECLARE @SecondMenuItemID INT
	DECLARE @SecondParentID INT
	DECLARE @SecondMenuItem VARCHAR(50)
	DECLARE @SecondMenuItemController VARCHAR(50)
	DECLARE @SecondMenuItemView VARCHAR(50)	
	DECLARE @SecondView BIT
	DECLARE @SecondAdd BIT
	DECLARE @SecondEdit BIT
	DECLARE @SecondDelete BIT
	DECLARE @SecondDetail BIT
	DECLARE @SecondDispOrder int	
	DECLARE @SecondOrderID BIT
	DECLARE @SecondRoleID BIT
	DECLARE @SecondIsActive BIT


	DECLARE Cur_Parent CURSOR FOR SELECT MenuItemID, MenuItem,MenuItemController,MenuItemView,ParentID,SortOrder,IsActive FROM tblMenuItem WHERE ParentID = 0 AND IsCompanyAdminType = @Type  AND IsActive = 1 ORDER BY SortOrder
	OPEN Cur_Parent
	
	FETCH NEXT FROM Cur_Parent INTO @MenuItemID,@MenuItem,@MenuItemController,@MenuItemView,@ParentID,@SortOrder,@IsActive	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF EXISTS(SELECT * FROM tblRolePrivileges WHERE MenuItemID = @MenuItemID AND RoleID = @RoleID)--AND RoleID = @RoleId
		BEGIN
			SELECT @View = ISNULL([View],0), @Add = ISNULL([Add],0), @Edit = ISNULL([Edit],0), @Delete = ISNULL([Delete],0), @Detail = ISNULL(Detail,0)
			FROM tblRolePrivileges WHERE MenuItemID = @MenuItemID AND RoleId = @RoleID
			
		END
		ELSE
		BEGIN
			SET @View = 0
			SET @Add = 0 
			SET @Edit = 0
			SET @Delete = 0
			SET @Detail = 0
		END
		
		INSERT INTO #TEMP1 VALUES(@MenuItemID,@MenuItem, @MenuItemController,@MenuItemView, @View, @Add, @Edit, @Delete,@Detail,@MenuItemID,@ParentID,@ParentID,@SortOrder,1,@RoleID)
		
		DECLARE CurFirst_Child CURSOR FOR SELECT MenuItemID, MenuItem,MenuItemController,MenuItemView,ParentID,SortOrder,IsActive FROM tblMenuItem WHERE ParentID = @MenuItemID AND IsCompanyAdminType = @Type AND IsActive = 1 ORDER BY SortOrder
		OPEN CurFirst_Child
			FETCH NEXT FROM CurFirst_Child INTO @FirstMenuItemID,@FirstMenuItem,@FirstMenuItemController,@FirstMenuItemView,@FirstParentID,@FirstDispOrder,@FirstIsActive
			--FETCH NEXT FROM CurFirst_Child INTO @FirstMenuItemID,@FirstParentID, @FirstMenuItem, @FirstMenuItemView,@FirstDispOrder
			
			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF EXISTS(SELECT * FROM tblRolePrivileges WHERE MenuItemID = @FirstMenuItemID AND RoleId = @RoleID)--AND RoleId = @RoleId
			BEGIN
				SELECT @FirstView = ISNULL([View],0), @FirstAdd = ISNULL([Add],0), @FirstEdit = ISNULL([Edit],0), @FirstDelete = ISNULL([Delete],0), @FirstDetail = ISNULL(Detail,0)
				FROM tblRolePrivileges WHERE MenuItemID = @FirstMenuItemID  AND RoleId = @RoleID
			END
			ELSE
				BEGIN
				SET @FirstView = 0
				SET @FirstAdd = 0 
				SET @FirstEdit = 0
				SET @FirstDelete = 0
				SET @FirstDetail = 0
				END			
			
				INSERT INTO #TEMP1 VALUES(@FirstMenuItemID,@FirstMenuItem, @FirstMenuItemController,@FirstMenuItemView, @FirstView, @FirstAdd, @FirstEdit, @FirstDelete,@FirstDetail,@FirstMenuItemID,@FirstParentID,@FirstParentID,@FirstDispOrder,1,@RoleID)
				
					DECLARE CurSecond_Child CURSOR FOR SELECT MenuItemID, MenuItem,MenuItemController,MenuItemView,ParentID,SortOrder,IsActive FROM tblMenuItem  WHERE ParentID = @FirstMenuItemID AND IsActive = 1 ORDER BY MenuItem
					OPEN CurSecond_Child
					
						--FETCH NEXT FROM CurSecond_Child INTO @SecondMenuItemID,@SecondParentID, @SecondMenuItem, @SecondMenuItemView,@SecondDispOrder
						FETCH NEXT FROM CurSecond_Child INTO @SecondMenuItemID,@SecondMenuItem,@SecondMenuItemController,@SecondMenuItemView,@SecondParentID,@SecondDispOrder,@SecondIsActive
						
						WHILE @@FETCH_STATUS = 0
						BEGIN
							IF EXISTS(SELECT * FROM tblRolePrivileges WHERE MenuItemID = @SecondMenuItemID AND RoleId = @RoleID)--AND RoleId = @RoleId
						BEGIN
							SELECT @SecondView = ISNULL([View],0), @SecondAdd = ISNULL([Add],0), @SecondEdit = ISNULL([Edit],0), @SecondDelete = ISNULL([Delete],0), @SecondDetail = ISNULL(Detail,0)
							FROM tblRolePrivileges WHERE MenuItemID = @SecondMenuItemID  AND RoleId = @RoleID 
						END
						ELSE
							BEGIN
							SET @SecondView = 0
							SET @SecondAdd = 0 
							SET @SecondEdit = 0
							SET @SecondDelete = 0
							SET @SecondDetail = 0
							END			
						
							--INSERT INTO #TEMP1 VALUES(@SecondMenuItemID,@SecondParentID, @SecondMenuItem, @SecondMenuItemView, @SecondView, @SecondAdd, @SecondEdit, @SecondDelete,@SecondDetail,@SecondDispOrder,@SecondMenuItemID,@RoleID)
							INSERT INTO #TEMP1 VALUES(@SecondMenuItemID,@SecondMenuItem, @SecondMenuItemController,@SecondMenuItemView, @SecondView, @SecondAdd, @SecondEdit, @SecondDelete,@SecondDetail,@SecondMenuItemID,@FirstParentID,@SecondParentID,@SecondDispOrder,1,@RoleID)
							
							--FETCH NEXT FROM CurSecond_Child INTO @SecondMenuItemID, @SecondParentID,@SecondMenuItem, @SecondMenuItemView,@SecondDispOrder 
							FETCH NEXT FROM CurSecond_Child INTO @SecondMenuItemID,@SecondMenuItem,@SecondMenuItemController,@SecondMenuItemView,@SecondParentID,@SecondDispOrder,@SecondIsActive
						END			
					
					CLOSE CurSecond_Child
					DEALLOCATE CurSecond_Child
				
				
				--FETCH NEXT FROM CurFirst_Child INTO @FirstMenuItemID, @FirstParentID,@FirstMenuItem, @FirstMenuItemView,@FirstDispOrder 
				FETCH NEXT FROM CurFirst_Child INTO @FirstMenuItemID,@FirstMenuItem,@FirstMenuItemController,@FirstMenuItemView,@FirstParentID,@FirstDispOrder,@FirstIsActive	
			END			
		
		CLOSE CurFirst_Child
		DEALLOCATE CurFirst_Child
		
		--FETCH NEXT FROM Cur_Parent INTO @MenuItemID,@ParentID, @MenuItem, @MenuItemView,@SortOrder	
		FETCH NEXT FROM Cur_Parent INTO @MenuItemID,@MenuItem,@MenuItemController,@MenuItemView,@ParentID,@SortOrder,@IsActive	
	END
	
	CLOSE Cur_Parent
	DEALLOCATE Cur_Parent

	SELECT * FROM #Temp1

END

/****** Object:  StoredProcedure [dbo].[GetPlantHeader]    Script Date: 01/09/2014 15:17:32 ******/
SET ANSI_NULLS ON





GO
