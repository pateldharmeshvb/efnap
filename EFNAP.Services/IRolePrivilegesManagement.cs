﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFNAP.Services
{
    interface IRolePrivilegesManagement
    {
        List<int?> GetParentIDsByRole(int roleID);
    }
}
