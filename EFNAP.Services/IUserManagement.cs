﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFNAP.Services
{
    interface IUserManagement
    {
        IList<EFNAP.Domain.vwRolePrivileges> GetMainMenuList(int RoleID, List<int?> ParentList);
        IList<EFNAP.Domain.vwRolePrivileges> GetMenuList(int RoleID);
        //IEnumerable<DocumentOutputDto> GetDocuments(int caseId, string appID);
        //IEnumerable<LookUpDto> GetDocumentType(string caseType);

        //long AddDocument(string appType, DocumentInputDto DocumentInput);
        //long AddDocument(int caseId, Byte[] file);
        //IEnumerable<DocOutputDto> GetDocument(int iDocumentID);
        //int RemoveDocument(int iDocumentID);
    }
}
