﻿using EFNAP.Domain;
using EFNAP.Infrastructure;
using EFNAP.Repositories.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;

namespace EFNAP.Services
{
    public class RolePrivilegesManagement : IRolePrivilegesManagement 
    {
        #region Get ParentIDs By Role
        /// <summary>
        /// Get menu ParentID list role wise
        /// </summary>
        /// <param name="roleID"></param>
        /// <returns>Reurns list of int</returns>
        public List<int?> GetParentIDsByRole(int roleID)
        {
            using (EFNAPContext ctx = new EFNAPContext())
            {
                List<int?> parentList = (from p in ctx.vwRolePrivileges
                                         where p.RoleID == roleID && p.View == true && p.ParentID > 0
                                         && p.IsActive == 1
                                         select p.ParentID).ToList();
                return parentList;
            }
        }
        #endregion
    }
}
