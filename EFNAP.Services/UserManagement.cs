﻿using EFNAP.Domain;
using EFNAP.Infrastructure;
using EFNAP.Repositories.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;

namespace EFNAP.Services
{
    public class UserManagement : IUserManagement 
    {
        #region Get Main Menu List
        /// <summary>
        /// Get Main Menu List
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns>Returns user data</returns>
        public IList<vwRolePrivileges> GetMainMenuList(int RoleID, List<int?> ParentList)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                
                IList<vwRolePrivileges> objMainMenu = context.vwRolePrivileges.Where(p => p.ParentID == 0 && p.RoleID == RoleID
                    && p.IsCompanyAdminType == false && ParentList.Contains(p.MenuItemID)).OrderBy(p => p.SortOrder).ToList();
                return objMainMenu;
            }
        }
        #endregion

        #region Get Menu List
        /// <summary>
        /// Get Main Menu List
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns>Returns user data</returns>
        public IList<vwRolePrivileges> GetMenuList(int RoleID)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                IList<vwRolePrivileges> objMenuList = context.vwRolePrivileges.Where(p => p.RoleID == RoleID && p.View == true && p.ParentID != 0 && p.IsCompanyAdminType == false
                                                    && p.IsActive == 1).OrderBy(p => p.OrderID).ThenBy(p => p.ParentID).ThenBy(p => p.SortOrder).ThenBy(p => p.MenuItem).ToList();
                return objMenuList;
            }
        }
        #endregion
        //public IEnumerable<LookUpDto> GetDocumentType(string caseType)
        //{
        //    string spName = "Sproc_GetDocumentType";
        //    return EFHelper.ExecuteSP<LookUpDto>(spName);
        //}

        //public long AddDocument(string appType, DocumentInputDto DocumentInput)
        //{
        //    int returnIndex = 3;
        //    SqlParameter[] paramArray = new SqlParameter[10];
        //    paramArray[0] = new SqlParameter("@CaseID", SqlDbType.Int);
        //    paramArray[0].Direction = ParameterDirection.Input;
        //    paramArray[0].Value = DocumentInput.CaseID;

        //    paramArray[1] = new SqlParameter("@sFileData", SqlDbType.VarBinary);
        //    paramArray[1].Direction = ParameterDirection.Input;
        //    paramArray[1].Value = DocumentInput.Document;

        //    paramArray[2] = new SqlParameter("@sFileDescription", SqlDbType.VarChar);
        //    paramArray[2].Direction = ParameterDirection.Input;
        //    paramArray[2].Value = DocumentInput.DocumentDesc;


        //    string spName = "Sproc_DocumentUpload_SAVE";
        //    return EFHelper.ExecuteSqlCommand(spName, paramArray, returnIndex);
        //}
    }
}
