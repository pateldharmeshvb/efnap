﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFNAP.Infrastructure;
using EFNAP.Infrastructure.DataContextStorage;

namespace EFNAP.Repositories.EntityFramework
{
    public static class DataContextFactory
    {
        /// <summary>
        /// Clears out the current ContactManagerContext.
        /// </summary>
        public static void Clear()
        {
            var dataContextStorageContainer = DataContextStorageFactory<EFNAPContext>.CreateStorageContainer();
            dataContextStorageContainer.Clear();
        }

        /// <summary>
        /// Retrieves an instance of ContactManagerContext from the appropriate storage container or
        /// creates a new instance and stores that in a container.
        /// </summary>
        /// <returns>An instance of ContactManagerContext.</returns>
        public static EFNAPContext GetDataContext()
        {
            var dataContextStorageContainer = DataContextStorageFactory<EFNAPContext>.CreateStorageContainer();
            var LegalContext = dataContextStorageContainer.GetDataContext();
            if (LegalContext == null)
            {
                LegalContext = new EFNAPContext();
                dataContextStorageContainer.Store(LegalContext);
            }
            return LegalContext;
        }
    }
}
