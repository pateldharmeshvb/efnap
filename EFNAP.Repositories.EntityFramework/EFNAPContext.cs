﻿namespace EFNAP.Repositories.EntityFramework
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public partial class EFNAPContext : DbContext, IDisposable
    {
        public EFNAPContext()
            : base("name=EFNAPDBEntity")
        {
         Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //This method is called when the model for a derived context has been initialized, but before the model has been locked down and used to initialize the context. The default implementation of this method does nothing, but it can be overridden in a derived class such that the model can be further configured before it is locked down.
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();

            modelBuilder.Entity<EFNAP.Domain.User>().ToTable("tblUser");
            modelBuilder.Entity<EFNAP.Domain.MenuItem>().ToTable("tblMenuItem");
            modelBuilder.Entity<EFNAP.Domain.Role>().ToTable("tblRole");
            modelBuilder.Entity<EFNAP.Domain.RolePrivileges>().ToTable("tblRolePrivileges");
            modelBuilder.Entity<EFNAP.Domain.Country>().ToTable("tblCountry");
            modelBuilder.Entity<EFNAP.Domain.State>().ToTable("tblState");
            modelBuilder.Entity<EFNAP.Domain.City>().ToTable("tblCity");
        }

        public DbSet<EFNAP.Domain.User> User { get; set; }
        public DbSet<EFNAP.Domain.MenuItem> MenuItem { get; set; }
        public DbSet<EFNAP.Domain.Role> Role { get; set; }
        public DbSet<EFNAP.Domain.Country> Country { get; set; }
        public DbSet<EFNAP.Domain.State> State { get; set; }
        public DbSet<EFNAP.Domain.City> City { get; set; }
        public DbSet<EFNAP.Domain.RolePrivileges> RolePrivileges { get; set; }
        public DbSet<EFNAP.Domain.vwRolePrivileges> vwRolePrivileges { get; set; }
    }
}
