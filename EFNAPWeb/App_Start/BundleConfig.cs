﻿using System.Web;
using System.Web.Optimization;

namespace EFNAPWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/CSS/MainCommon").Include("~/CSS/Common/bootstrap.min.css",
                                                                "~/CSS/Common/bootstrap-theme.css",
                                                                "~/CSS/Common/kendo.common-bootstrap.min.css",
                                                                "~/CSS/Common/kendo.bootstrap.min.css",
                                                                "~/CSS/Common/kendo.uniform.min.css",
                                                                "~/CSS/fonts/fonts.css",
                                                                "~/CSS/Custom/style.css",
                                                                "~/CSS/Custom/font-awesome.min.css",
                                                                "~/CSS/Common/easy-responsive-tabs.css",
                                                                "~/CSS/Common/select2.min.css",
                                                                "~/CSS/Common/jquery.smartmenus.bootstrap.css",
                                                                "~/CSS/Common/bootstrap-switch.min.css",
                                                                "~/CSS/Common/jquery-ui-1.10.4.custom.min.css",
                                                                "~/CSS/Common/datepicker.css",
                                                                "~/CSS/Common/bootstrap-multiselect.css",
                                                                "~/CSS/Common/bootstrap-datetimepicker.css",
                                                                "~/CSS/Common/jquery.mCustomScrollbar.css",
                                                                "~/CSS/Common/bootstrap-dialog.min.css"
                                                               ));


            bundles.Add(new StyleBundle("~/CSS/FrontCommon").Include("~/CSS/Common/bootstrap.min.css",
                                                                "~/CSS/Common/bootstrap-theme.css",
                                                                "~/CSS/fonts/fonts.css",
                                                                "~/CSS/Custom/Style.css"
                                                               ));


            bundles.Add(new ScriptBundle("~/Scripts/FrontCommon").Include(
                                                                  "~/Scripts/Common/jquery-2.1.4.min.js",
                                                                  "~/Scripts/Common/bootstrap.min.js",
                                                                  "~/Scripts/Common/jquery.validate.min.js",
                                                                  "~/Scripts/Common/jquery.validate.unobtrusive.min.js"                                                                    
                                                                  ));           

            bundles.Add(new ScriptBundle("~/Scripts/TopScript").Include(
                                                                    "~/Scripts/Common/jquery-2.1.4.min.js",
                                                                    "~/Scripts/Custom/SiteUrl.js"
                                                                  ));

            bundles.Add(new ScriptBundle("~/Scripts/CommonAll").Include(
                                                                    "~/Scripts/Common/twitter-bootstrap-hover-dropdown.min.js",
                                                                    "~/Scripts/Common/bootstrap.min.js",
                                                                    "~/Scripts/Common/bootstrap-dialog.min.js",
                                                                    "~/Scripts/Common/jquery.hoverIntent.minified.js",
                                                                    "~/Scripts/Common/jquery.validate.min.js",
                                                                    "~/Scripts/Common/jquery.validate.unobtrusive.min.js",
                                                                    "~/Scripts/Common/modernizr-*",
                                                                    "~/Scripts/Common/jquery.smartmenus.js",
                                                                    "~/Scripts/Common/jquery.smartmenus.bootstrap.js",
                                                                    "~/Scripts/Common/respond.min.js",
                                                                    "~/Scripts/Common/jquery-ui-1.10.4.custom.min.js",
                                                                    "~/Scripts/Common/bootstrap-multiselect.js",
                                                                    "~/Scripts/Common/jquery.unobtrusive-ajax.min.js",
                                                                    "~/Scripts/Common/jquery.placeholder-enhanced.min.js",
                                                                    "~/Scripts/Common/jquery.easing.min.js",
                                                                    "~/Scripts/Common/kendo.all.min.js",
                                                                    "~/Scripts/Common/kendo.aspnetmvc.min.js",
                                                                    "~/Scripts/Common/jquery.form.min.js",
                                                                    "~/Scripts/Custom/passwordStrengthMeter.js",
                                                                    "~/Scripts/Common/jquery.maskMoney.js",
                                                                    "~/Scripts/Common/moment.min.js",
                                                                    "~/Scripts/Common/bootstrap-datetimepicker.js",
                                                                    "~/Scripts/Common/bootstrap-switch.min.js",
                                                                    "~/Scripts/Common/jquery.mCustomScrollbar.concat.min.js",
                                                                    "~/Scripts/Common/easy-responsive-tabs.js",
                                                                    "~/Scripts/Custom/common.js",
                                                                    "~/Scripts/Custom/CommonMessages.js"
                                                                    ));

            bundles.Add(new ScriptBundle("~/Scripts/validate").Include("~/Scripts/Common/jquery.validate.min.js",
                "~/Scripts/Common/jquery.validate.unobtrusive.min.js",
                   "~/Scripts/Common/jquery.unobtrusive-ajax.min.js"
                   ));

            bundles.Add(new ScriptBundle("~/Scripts/Document").Include(
                "~/Scripts/Common/easy-responsive-tabs.js",
                "~/Scripts/Common/select2.full.js"));
        
        }
    }
}
