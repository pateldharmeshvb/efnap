﻿$(function () {
    $('#frmTaskComplete').submit(function (e) {
        var form = $(this);
        if (form.validate().form()) {
            $.post(SiteUrl + "Home/CompleteTask",
                form.serialize(),
                function (data) {
                    if (data.Success) {
                        $("#TaskCompletion").modal('hide')
                        RefreshTaskGrid();
                    }
                });
        }
        e.preventDefault();
    });
    $("#taskFilterType").change(function () {
        RefreshTaskGrid();
    });
    $('#frmTaskApprove').submit(function (e) {
        var form = $(this);
        if (form.validate().form()) {
            $.post(SiteUrl + "Home/ApproveTask",
                    form.serialize(),
                    function (data) {
                        if (data.Success) {
                            $("#TaskApprove").modal('hide')
                            RefreshTaskGrid();
                        }
                    });
        }
        e.preventDefault();
    });
    $('#frmTaskDecline').submit(function (e) {
        var form = $(this);
        if (form.validate().form()) {
            $.post(SiteUrl + "Home/DeclineTask",
                    form.serialize(),
                    function (data) {
                        if (data.Success) {
                            $("#TaskDecline").modal('hide')
                            RefreshTaskGrid();
                        }
                    });
        }
        e.preventDefault();
    });
    $("#rdNewDocument").click(function () {
        $("#uploaddoc").modal('show');
        $("#uploaddoc").load(SiteUrl + "Home/UploadDocument");
        $("#hdnUploadFrom").val("1");
    });
    $("#rdExistingDocument").click(function () {
        $("#selectdocument").modal('show');
        $("#selectdocument").load(SiteUrl + "Home/SelectDocument");
    })
    $("#btnRemoveFile").click(function () {
        $("#divCompleted").hide();
        $("#divFileUpload").show();
        $("#DocumentID").val("");
        $("#lblFileName").html("");
    });
});
function RefreshTaskGrid() {
    var filterValue = $("#taskFilterType").val();
    var grid = $('#gridMyTask').data('kendoGrid');
    grid.dataSource.transport.options.read.url = SiteUrl + "Home/MyTasks_Read?filterValue=" + filterValue;
    grid.dataSource.read();

}
function TaskAdditionInfoDialog(taskID, taskName) {
    $("#TaskAdditionalInformation").modal('show');
    loadTaskAdditionalInfo(taskID, taskName);
}
function TaskCompleteDialog(taskID, taskName) {
    $("#CompleteTaskId").val(taskID);
    $("#lblCompleteTaskName").text(taskName);
    $("#CompleteRemarks").val("");
    $('#CompleteErrorMessage').html('');
    if (!$('#CompleteErrorMessage').hasClass("displaynone"))
        $('#CompleteErrorMessage').addClass("displaynone")
    if ($("#DocumentID") != null)
        $("#DocumentID").val("");
    if ($("#lblFileName") != null)
        $("#lblFileName").html("");
    if ($("#divFileUpload") != null)
        $("#divFileUpload").show();
    if ($("#divCompleted") != null)
        $("#divCompleted").hide();
    $("#TaskCompletion").modal('show');
}
function TaskApproveDialog(taskID, taskName) {
    $("#ApproveTaskId").val(taskID);
    $("#lblApproveTaskName").text(taskName);
    $("#ApproveRemarks").val("");
    $('#ApproveErrorMessage').html('');
    if (!$('#ApproveErrorMessage').hasClass("displaynone"))
        $('#ApproveErrorMessage').addClass("displaynone")
    $("#TaskApprove").modal('show');
}
function TaskDeclineDialog(taskID, taskName) {
    $("#DeclineTaskId").val(taskID);
    $("#lblDeclineTaskName").text(taskName);
    $("#DeclineRemarks").val("");
    $('#DeclineErrorMessage').html('');
    if (!$('#DeclineErrorMessage').hasClass("displaynone"))
        $('#DeclineErrorMessage').addClass("displaynone")

    $("#TaskDecline").modal('show');
}
function NewGridRebind(e) {
    return {
        SearchTypeID: $("#SearchTypeID").val(),
        SearchCriteria: $("input:radio[name='SearchCriteria']:checked").val(),
        SearchWord: $("#SearchWord").val(),
        ParentDocumentID: $("#DashboardFilesSearchTree").jstree('get_selected')[0],
        BelongsTo: $("#DashboardFilesSearchTree").jstree('get_selected')[0],
    };
}
function SearchDocuments() {
    $("#NewGrid").data("kendoGrid").dataSource.read();
    $("#NewGrid").data("kendoGrid").refresh();
}
function validateDeclineTask() {
    var strErrMsg = "";
    var isValid = true;
    strErrMsg += "<ul>";
    if (document.getElementById('DeclineRemarks').value == "") {
        strErrMsg += '<li>Remarks is required.</li>';
        isValid = false;
    }
    if (!isValid) {
        $('#DeclineErrorMessage').removeClass("displaynone")
        $('#DeclineErrorMessage').html(strErrMsg + '</ul>');        
        return false;
    }
    else {
        $('#DeclineErrorMessage').html('');
        $('#DeclineErrorMessage').addClass("displaynone")
        return true;
    }
}
function onSignalRGridDataBound(e) {
    if (!e.sender.dataSource.view().length) {
        var colspan = e.sender.thead.find("th:visible").length,
            emptyRow = '<tr><td colspan="' + colspan + '" align="center">No records found.</td></tr>';
        e.sender.tbody.parent().width(e.sender.thead.width()).end().html(emptyRow);
    }
    $("table.k-focusable tbody tr").hover(
      function () {
          $(this).toggleClass("k-state-hover");
      }
    );
    $("#MyTaskCount").html(e.sender.dataSource.view().length);
}
function MyTaskGridDataBound() {
    $.get(SiteUrl + "Home/GetMyTasksCount", {}, function (data) {
        $("#MyTaskCount").text(data.MyTaskCount);
    }, "json");
}
function LoadDocumentChart(officeDocCount, imagesCount, OtherDocCount) {
    var c = $('#myChart');
    var officeDoclabel = 'Office Documents (' + officeDocCount + ')';
    var imageslabel = 'Images (' + imagesCount + ')';
    var otherDoclabel = 'Other Files (' + OtherDocCount + ')';
    if (c != null) {
        var chartData = [
        {
            value: officeDocCount,
            color: "#FF7878",
            highlight: "#34495E",
            label: officeDoclabel
        },
        {
            value: imagesCount,
            color: "#528CC4",
            highlight: "#34495E",
            label: imageslabel
        },
        {
            value: OtherDocCount,
            color: "#2ECA70",
            highlight: "#34495E",
            label: otherDoclabel
        }
        ]
        var chartOptions = {
            animation: false,
            tooltipFontSize: 10,
            tooltipTemplate: "<%if (label){%><%=label %><%}%>",
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        if (c.get(0) != null) {
            var ct = c.get(0).getContext('2d');
            var ctx = document.getElementById("myChart").getContext("2d");
            var myDoughnutChart = new Chart(ct).Doughnut(chartData, chartOptions);
            var legend = myDoughnutChart.generateLegend();
            if ($("#legend") != null);
            $("#legend").html(legend);
        }
    }
}