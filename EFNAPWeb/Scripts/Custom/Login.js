﻿$(function () {
    // jQuery to collapse the navbar on scroll
    $(window).scroll(function () {
        if ($(".navbar").offset().top > 50) {
            $(".navbar-fixed-top").addClass("top-nav-collapse");
        } else {
            $(".navbar-fixed-top").removeClass("top-nav-collapse");
        }
    });

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function () {
        $('a.page-scroll').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });
    $("#loginbtn").click(function () {
        $("#login").show();
        $("#signup").hide();

        $("#signupbtn").parent().removeClass('active');
        $(this).parent().addClass('active');

        OpenSignInOrJoin();
    });
    $("#signupbtn").click(function () {
        $("#login").hide();
        $("#signup").show();

        $("#loginbtn").parent().removeClass('active');
        $(this).parent().addClass('active');
        OpenSignInOrJoin();
    });
    $("#RegistrationUserTypeID").change(function () {
        var drpdownValue = $(this).val();
        OpenSignInOrJoin();
        $("#RegistrationUserTypeID").val(drpdownValue);
    });


    $('#Password').keyup(
        function () {
            $('#passComment').html(passwordStrength($('#Password').val(), 'UserName'));
            if ($('#passComment').html() == "&nbsp;&nbsp;Too short" || $('#passComment').html() == "&nbsp;&nbsp;Average") {
                document.getElementById("passComment").style.color = 'red';
            }
            else {
                document.getElementById("passComment").style.color = 'green';
            }
        }
    )
});

function OpenSignIn() {
    $("#loginbtn").click();
    OpenSignInOrJoin();
}
function OpenSignInOrJoin() {
    $('#myModal').find('input:text').val('');
    $('#myModal').find('input:password').val('');
    $('#myModal').find('select').val('');
    $('#myModal').find('input:text').removeClass('input-validation-error');
    $('#myModal').find('input:password').removeClass('input-validation-error');
    $('#myModal').find('select').removeClass('input-validation-error');
    $('#myModal').find('.errortext').find('span').html('');
    $('#passComment').text('');
    $('#messageLogin').hide();
    $('#messageLogin').html('');
    $('#divSigninSuccess').addClass('displaynone');
    $('#divLoginError').addClass('displaynone');
    $('#divResendError').addClass('displaynone');
    $('#divResendSuccess').addClass('displaynone');
    $('#divNeedToResend').addClass('displaynone');
    $('#divRegistrationCompleteSuccess').addClass('displaynone');


    $("#signup").find('div').removeClass("displaynone");
    $("#divVerify").addClass('displaynone');
    $("#divVerify").find('div').addClass('displaynone');
    $("#divNeedToResend").addClass('displaynone');
    $("#divNeedToResend").find('div').addClass('displaynone');
    $("#divSigninSuccess").addClass('displaynone');
    $("#divSigninError").addClass('displaynone');
    $('#divRegistrationError').addClass('displaynone');
    $('#divResendNewAccountSuccess').addClass('displaynone');

    $('#myModal').modal('show');
}
function OpenJoin(btn) {
    if (btn == 'login') {
        $("#loginbtn").click();
    }
    else {
        $("#signupbtn").click();
    }
}
function validateLogin() {
    var errorMessage = '<ul>';
    var isvalid = true;
    if ($('#UserTypeID').val().trim() == '') {
        errorMessage += '<li>Please select user type.</li>';
        isvalid = false;
    }

    if ($('#Email').val().trim() == '') {
        errorMessage += '<li>Please enter email.</li>';
        isvalid = false;
    }
    else if ($('#Email').val().trim() != '' && !validateEmail($('#Email').val().trim())) {
        errorMessage += '<li>Invalid email address.</li>';
        isvalid = false;
    }

    if ($('#UserPassword').val().trim() == '') {
        errorMessage += '<li>Please enter password.</li>';
        isvalid = false;
    }

    if (isvalid == false) {

        $('#messageLogin').show();
        $('#messageLogin').html(errorMessage + '</ul>');
        if ($('#msgUnAuthorized') != null)
            $('#msgUnAuthorized').hide();
        return false;
    }
    else {
        return true;
    }
}

//Added By DIpak B. Kansara
//Added funbction to valid email instead of using common js because to include common js we need to include other file
function validateEmail(field) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (field.match(mailformat)) {
        return true;
    }
    else {
        return false;
    }
}

function ResendEmail() {
    var siteBaseUrl = SiteUrl + "Home/ResendEmail";
    var jsonPostData = { UserID: $("#UserID").val() };
    $.ajax({
        type: "POST",
        url: siteBaseUrl,
        data: JSON.stringify(jsonPostData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.success == "1") {
                window.location = "Home/Index?Msg=ResendEmail";
            }
        }
    });
}
function ResendEmailForNewAccount() {
    var siteBaseUrl = SiteUrl + "Home/ResendEmailForNewAccount";
    $.ajax({
        type: "POST",
        url: siteBaseUrl,
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.success == "1") {
                window.location = "Home/Index?Msg=ResendEmailNewAccount";
            }
        }
    });
}

function DisableCutCopyPaste() {
    $("input[type='password']").bind("cut copy paste", function (e) {
        e.preventDefault();
    });
}