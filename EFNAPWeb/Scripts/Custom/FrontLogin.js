﻿$(function () {
    DisableCutCopyPaste();
});
function validateLogin() {
    var errorMessage = '';
    var isvalid = true;
    if ($('#UserName').val().trim() == '') {
        errorMessage += 'Please enter username.<br/>';
        isvalid = false;
    }
    if ($('#UserPassword').val().trim() == '') {
        errorMessage += 'Please enter password.';
        isvalid = false;
    }
    if (isvalid == false) {
        $('#messageLogin').show();
        $('#messageLogin').html(errorMessage);
        if ($('#msgUnAuthorized') != null)
            $('#msgUnAuthorized').hide();
        return false;
    }
    else {
        return true;
    }
}
function DisableCutCopyPaste() {
    $("input[type='password']").bind("cut copy paste", function (e) {
        e.preventDefault();
    });
}