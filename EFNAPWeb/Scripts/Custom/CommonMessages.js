﻿
var _RequiredField = "##FieldName## is required.";
var _ValidFileExtention = "You can only upload files like .xls, .xlsx.";
var _RequiredData = "Please enter data to import file.";
var _ReadingFile = "Reading uploaded file...";
var _StartFileProcess = "File process starting...";
var _NotFound = "##FieldName## not found.";
var _ValidationCompleted = "Validation process has been completed and starting to process file...";
var _RecordCount = "Validating records ##MsgRecordCount## out of ##MsgTotalRecord##";
var _ErrorValidating = "Some error occurred while validating file...";
var _FileProcessCompleted = "File process has been completed....";
var _FileProcessing = "Processing records ##MsgRecordCount## out of ##MsgTotalRecord##";
var _ErrorProcessing = "Some error occurred while processing file...";
var _PasswordNotMatch = "Passwords do not match.";
var _NotConfirmed = "Not Confirmed";
var _Confirmed = "Confirmed";
var _UsernameAvailable = "Congratulations! The username is available.";
var _UsernameNotAvailable = "Sorry! The username is already taken.";
var _AlreadyExists = "##FieldName## already exists. Try to choose different value.";
var _FromDateToDate = "From date should be less than or equal to - To date.";
var _YearDays = "Days value should be between 1 to 365.";
var _SelectBelongTo = "Please select folder from belongs to.";
var _Error = "Some error occured, Please try again.";
var _DocumentUpdate = "Document updated successfully.";
var _DocumentAdded = "Document added successfully.";
var _DocumentShare = "Document shared successfully.";
var _DocumentSharewithUser = "Shared user rights updated successfully.";
var _SelectDocument = "Please select at least 1 user to share.";
var _DocumentShareDelete = "Sharing remove successfully.";
var _DocumentShareConfirm = "Are you sure ? Do you want to remove sharing.";
var _TaskAdded = "Task added successfully.";


var _SelectFile = "Please select file to upload.";
var _SelectFileG = "અપલોડ કરવા માટે ફાઇલ પસંદ કરો.";

var _SelectForSearch = "Please select search criteria.";
var _SelectForSearchG = "શોધ માટે વિકલ્પ પસંદ કરો.";

var _InValidFileExtention = "You can only upload files like #Extentions#.";
var _InValidFileExtentionG = "તમે માત્ર #Extentions# પ્રકાર ની ફાઇલો અપલોડ કરી શકો છો";