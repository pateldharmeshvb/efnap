$.ajaxSetup({
    error: function (x, e) {
        if (x.status == 403) {
            window.location = SiteUrl + "/Home/Index";
        }
    }
});
$(document).ready(function () {
    $('form').submit(function () {
        var FormID = $(this).attr("id");
        if (FormID != "RolePrivileges" && FormID != "FolderPrivileges" && FormID != "frmTaskComplete" && FormID != "frmTaskApprove" && FormID != "frmTaskDecline") {
            if ($(this).find('.input-validation-error').length == 0) {
                $(this).find(':submit').attr('disabled', 'disabled');
            }
        }
    });
    var el = $("input:text").get(0);
    if (el != null) {
        var elemLen = el.value.length;
        el.selectionStart = elemLen;
        el.selectionEnd = elemLen;
        el.focus();
    }
    $(".IsActiveStatus").bootstrapSwitch({
        'size': 'small',
        'onText': 'Active',
        'offText': 'Inactive',
        'labelText': 'Inactive',
        'offColor': 'primary'
    });
    $(".bootstrap-switch-label").css('background-color', '#E8E8E8');
    $(".IsActiveStatus").on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $(this).parents("form").find(".bootstrap-switch-label").text('Inactive');
        }
        else {
            $(this).parents("form").find(".bootstrap-switch-label").text('Active');
        }
    });
    if ($(".IsActiveStatus") != null) {
        if ($(".IsActiveStatus").is(':checked')) {
            $(".bootstrap-switch-label").text('Inactive');
        }
        else {
            $(".bootstrap-switch-label").text('Active');
        }
    }
    $('[data-toggle=offcanvas]').on('click', function (e) {
        if ($('.row-offcanvas').hasClass('active')) {
            openCloseSidebar('close');
        } else {
            openCloseSidebar('open');
        }
    });
    $('.fab .dropdown-menu li a').on('click', function () {
        var $target = $(this).attr('data-modal');
        $($target).modal();
    });
    checkSidebar();
    /* Annoucement */
    $('.anounce-wrp').find('#viewalllink').on('click', function (e) {
        e.preventDefault();
        this.expand = !this.expand;
        $(this).html(this.expand ? "View Less <i class='fa fa-angle-double-up'></i>" : "View All <i class='fa fa-angle-double-down'></i>");
        $(this).closest('.anounce-wrp').find('.announcementbox, .announcementbox-big').toggleClass('announcementbox announcementbox-big');
    });
    /* File Upload */
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });
    $('.btn-file :file').on('fileselect', function (event, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = label;
        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function hidenotepad() {
        $("#sidebar").css("z-index", "");
        if ($(".right-popup").is(':visible')) {
            console.log("rightpopup visible");
            $('.right-popup:visible').hide("slow");
        }
    }
    $('.headerbg, .rightpartspace, .meterbox, .birthlistbox, .closenotepad').not(".rightpopup").click(function (e) {
        hidenotepad();
    });
    $(".rightpopup").click(function () {
        hidenotepad();
        var id = $(this).attr('title');
        if ($("#" + id).is(':visible')) {
            $("#" + id).hide("slow");
            $("#sidebar").css("z-index", "");
        } else {
            $("#" + id).show("slow");
            $("#sidebar").css("z-index", "1000");
            if (id == "Favorites") {
                window.setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: SiteUrl + "Home/GetAllFavorites",
                        data: {},
                        cache: false,
                        async: true,
                        success: function (GAFav) {
                            document.getElementById("divAllFavorites").innerHTML = GAFav.FavData;
                        }
                    });
                }, 1000);
            }
            if (id == "Notepad") {
                InitializeNote();
            }
            if (id == "Notifications") {
                window.setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: SiteUrl + "Home/GetNotifications",
                        data: {},
                        cache: false,
                        async: true,
                        success: function (result) {
                            var html = "<table class='table' width='100%'><thead><tr><th>Description</th></thead></tr>";
                            if (result.Data.length > 0) {
                                $.each(result.Data, function (index, value) {
                                    html += "<tr style='width:100%'><td>" + value + "</td></tr>";
                                });
                                $("#Notificationscount").html(result.Data.length);
                            }
                            else {
                                html += "<tr><td>You do not have any notification.</td></tr>";
                                $("#Notificationscount").html("0");
                            }
                            html += "</table>"
                            $("#divAllNotifications").html(html);
                        }
                    });
                }, 1000);
            }
        }
    });
    $(".inipad").click(function (e) {
        hidenotepad();
    });
    $("#imgInp").change(function () {
        readURL(this);
    });
    jQuery.validator.methods["date"] = function (value, element) {
        var isChrome = window.chrome;
        // make correction for chrome
        if (isChrome) {
            var d = new Date();
            return this.optional(element) ||
            !/Invalid|NaN/.test($.datepicker.parseDate("dd/mm/yyyy", value));
        }
        else {
            return this.optional(element) ||
            !/Invalid|NaN/.test(new Date(value));
        }
    }
    $('.fab').hover(function () {
        $(this).toggleClass('active');
    });
    $('[data-toggle="tooltip"]').tooltip();
    $(".remove").click(function () {
        $(".fab").fadeOut("fast");
    });
    $("[data-toggle=popover]").popover();
    $("#popoverExampleTwo").popover({
        html: true,
        content: function () {
            return $('#userdetails').html();
        },
    });
});
function openCloseSidebar(flag) {
    window.localStorage.setItem('sidebar', flag);
    if (flag == 'close') {
        $('.row-offcanvas').removeClass('active');
        $(".footerbg").removeClass("site-footer");
    } else {
        $('.row-offcanvas').addClass('active');
        $(".footerbg").addClass("site-footer");
    }
}
function checkSidebar() {
    var flag = window.localStorage.getItem('sidebar');
    if (flag !== null) {
        openCloseSidebar(flag);
    } else {
        if ($(window).width() > 767) {
            openCloseSidebar('open');
        } else {
            openCloseSidebar('close');
        }
    }
}

$(document).ajaxStart(function () {
    $("#loading").show();
});
$(document).ajaxStop(function () {
    $("#loading").hide();
});
function openPopup(iterator) {
    $("#announcementTitle").html($("#Name_" + iterator).html());
    $("#announcementDescription").html($("#Description_" + iterator).val());
    var FilePath = $("#File_" + iterator).val();
    if (FilePath != "" && FilePath != undefined && FilePath != null) {
        $("#announcementViewFile").attr("href", FilePath);
        $("#announcementViewFile").show();
    }
    else {
        $("#announcementViewFile").hide();
    }
    $("#AnnouncementPopup").modal('show');
}
function onGridDataBound(e) {
    if (!e.sender.dataSource.view().length) {
        var colspan = e.sender.thead.find("th:visible").length,
            emptyRow = '<tr><td colspan="' + colspan + '" align="center">No records found.</td></tr>';
        e.sender.tbody.parent().width(e.sender.thead.width()).end().html(emptyRow);
    }
    $("table.k-focusable tbody tr").hover(
      function () {
          $(this).toggleClass("k-state-hover");
      }
    );
}
function Check_CheckBox_Count(deleteName) {
    var $b = $('input[name=chkDelete]');
    var total_selected = $b.filter(':checked').length;
    if (total_selected > 0) {
        return confirm('Are you sure? Do you want to delete record(s)?');
    }
    else {
        alert("Please select at least one checkbox to delete.");
        return false;
    }
}
function InitializeNote() {
    $("#divNoteSavedMsg").hide();
    $.get(SiteUrl + "Home/GetUserNoteText", function (data) {
        $("#notes").val(data.notetext);
    });
    $("#notes").focus();
}
function initializeSingleCheckBox(id) {
    var isChecked = $('#' + id).is(':checked');
    $('#' + id).closest('tr').addClass(isChecked ? 'selected-row' : 'not-selected-row');
    $('#' + id).closest('tr').removeClass(isChecked ? 'not-selected-row' : 'selected-row');
    if (isChecked && $('.singleCheckBox').length == $('.selected-row').length)
        $('#allCheckBox').prop('checked', true);
    else
        $('#allCheckBox').prop('checked', false);
}
function initializeAllCheckBox() {
    var isChecked = $('#allCheckBox').is(':checked');
    $('.singleCheckBox').prop('checked', isChecked ? true : false);
    $('.singleCheckBox').closest('tr').addClass(isChecked ? 'selected-row' : 'not-selected-row');
    $('.singleCheckBox').closest('tr').removeClass(isChecked ? 'not-selected-row' : 'selected-row');
}
function onlyNumbers(e) {
    var keynum;
    var keychar;
    var charcheck;
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode == 8 || charCode == 9 || charCode == 13) {
        return true;
    }
    if (window.event) // IE
    {
        keynum = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which;
    }
    keychar = String.fromCharCode(keynum);
    charcheck = /[0-9]/;
    return charcheck.test(keychar);
}
function isUrlValid(url) {
    if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url)) {
        return true;
    } else {
        return false;
    }
}
$(window).resize(function () {
    if ($(window).width() > 767) {
        $('.row-offcanvas').toggleClass('active');
        $(this).find('i').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-left')
        var sideBarHeight = $(".row-offcanvas").height();
    }
});
String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}
function isNumberOnlyKey(element, evt) {
    var CursorIndex = GetCursorLocation(element);
    alert(CursorIndex)
    var charCode = (evt.which) ? evt.which : evt.keyCode
    var keyCode = evt.keyCode

    if (charCode > 95 && charCode < 106) {
        return true;
    }
    else if (charCode > 31 && ((charCode < 46 || charCode > 57) && charCode != 47 && keyCode != 9 && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40)) {
        return false;
    }
    else if (charCode == 47) {
        return false;
    }
    else
        return true;
}
function isNumberOnlyKeyWithOutZero(element, evt) {
    var CursorIndex = GetCursorLocation(element);
    var charCode = (evt.which) ? evt.which : evt.keyCode
    var keyCode = evt.keyCode
    if (CursorIndex == 0 && charCode == 48) {
        return false;
    }
    if (charCode > 95 && charCode < 106) {
        return true;
    }
    else if (charCode > 31 && ((charCode < 46 || charCode > 57) && charCode != 47 && keyCode != 9 && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40)) {
        return false;
    }
    else if (charCode == 47) {
        return false;
    }
    else
        return true;
}
function GetCursorLocation(CurrentTextBox) {
    var CurrentSelection, FullRange, SelectedRange, LocationIndex = -1;
    if (typeof CurrentTextBox.selectionStart == "number") {
        LocationIndex = CurrentTextBox.selectionStart;
    }
    else if (document.selection && CurrentTextBox.createTextRange) {
        CurrentSelection = document.selection;
        if (CurrentSelection) {
            SelectedRange = CurrentSelection.createRange();
            FullRange = CurrentTextBox.createTextRange();
            FullRange.setEndPoint("EndToStart", SelectedRange);
            LocationIndex = FullRange.text.length;
        }
    }
    return LocationIndex;
}
function loadAddRemove(id, className) {
    $(document).on('click', '.btn-add', function (e) {
        e.preventDefault();
        var controlForm = $('#' + id + ' div:first'),
            currentEntry = $(this).parents('.' + className + ':first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.' + className + ':not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function (e) {
        $(this).parents('.' + className + ':first').remove();
        e.preventDefault();
        return false;
    });
}
function populateDropdown(n, t) {
    $(n).children("option:not(:first)").remove();
    $.each(t, function (x, i) {
        n.append($("<option></option>").val(i.value).html(i.name));
    })
}