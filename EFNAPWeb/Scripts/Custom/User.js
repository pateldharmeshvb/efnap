﻿$(document).ready(function () {
    $('#Password').keyup(
        function () {
            $('#passComment').html(passwordStrength($('#Password').val(), $('#Username').val()));
            if ($('#passComment').html() == "&nbsp;&nbsp;Too short" || $('#passComment').html() == "&nbsp;&nbsp;Average") {
                document.getElementById("passComment").style.color = 'red';
            }
            else {
                document.getElementById("passComment").style.color = 'green';
            }
        }
    )
});

$(document).ready(function () {
    $("#Username").focus();
    //$.validator.setDefaults({
    //    onkeyup: false    
    //});

    var date = new Date();
    var currentMonth = date.getMonth();
    var currentDate = date.getDate();
    var currentYear = date.getFullYear();

    $('#DateOfBirth').datetimepicker({
        format: 'MM/DD/YYYY',
        maxDate: new Date(currentYear, currentMonth, currentDate),
        widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
    });

    $("#StateID").change(function () {
        BindCity();
    });
});
$(function () {
    $("#Password").change(function () {
        if (document.getElementById('Password').value.length > 0) {
            document.getElementById("spanConfirm").innerHTML = _NotConfirmed;//'Not Confirmed';
            document.getElementById("spanConfirm").style.color = 'red';
        }
        else {
            document.getElementById("spanConfirm").innerHTML = '';
        }
        return false;
    });
});

function funCofirmPassword() {
    if (document.getElementById('Password').value.length > 0 && document.getElementById('ConfirmPassword').value.length > 0 && document.getElementById('Password').value != document.getElementById('ConfirmPassword').value) {
        document.getElementById("spanConfirm").innerHTML = _PasswordNotMatch;//'Passwords do not match.';
        document.getElementById("spanConfirm").style.color = 'red';
    }
    else if (document.getElementById('Password').value.length > 0 && document.getElementById('ConfirmPassword').value.length == 0) {
        document.getElementById("spanConfirm").innerHTML = _NotConfirmed;//'Not Confirmed';
        document.getElementById("spanConfirm").style.color = 'red';
    }
    else {
        document.getElementById("spanConfirm").innerHTML = _Confirmed;//'Confirmed';
        document.getElementById("spanConfirm").style.color = 'green';
    }
}
//function checkusername() {    
//    var data = { UserName: $("#Username").val(), UserID: $('#UserID').val() };
//    var SiteBaseUrl = SiteUrl + "User/validateDuplicateRecord";
//    $.post(SiteBaseUrl, data, function (data) {
//        if (data.status == "0") {
//            document.getElementById("messageUser").style.display = '';
//            document.getElementById("btnSubmit").disabled = true;
//        }
//        else {
//            document.getElementById("messageUser").style.display = 'none';
//            document.getElementById("btnSubmit").disabled = false;
//        }
//    }, "json");
//}
function ValidateDuplicateUser(pagename, fieldName, textboxID, primaryKeyField, MessageFieldName, PrimaryKeyFieldName) {
    pagename = SiteUrl + pagename;
    var fieldNameArry = fieldName.split(",");
    var textBoxIDArry = textboxID.split(",");

    var strFieldList = "";
    var strValueList = "";

    for (var i = 0; i < fieldNameArry.length; i++) {
        if (strFieldList != "") {

            strFieldList += ",";
            strValueList += ",";
        }
        strFieldList += fieldNameArry[i];
        strValueList += $("#" + textBoxIDArry[i]).val();
    }
    var strEditID = -1;
    if (primaryKeyField != "add")
        strEditID = $("#" + primaryKeyField).val();
    if (strValueList.length > 0) {
        $.post(pagename,
    { FieldList: strFieldList, ValueList: strValueList, strAddOrEditID: strEditID, IDName: PrimaryKeyFieldName },
        function (data) {
            var myObject = eval(data);
            var newid;
            if (myObject.status != undefined)
                newid = myObject.status;
            else
                newid = myObject;
            if (newid == 1) {
                if (MessageFieldName == "User" && primaryKeyField == "add" && strValueList != "") {
                    $("#SuccessMessage").html(_UsernameAvailable);
                    $('#SuccessMessage').css('display', "");
                    $("#ErrorMessage").html("");
                    $('#ErrorMessage').css('display', "none");
                }
                else {
                    $("#SuccessMessage").html("");
                    $('#SuccessMessage').css('display', "none");
                    $("#ErrorMessage").html("");
                    $('#ErrorMessage').css('display', "none");
                }
                return true;
            }
            else if (newid == -1) {
                $("#ErrorMessage").html("");
                $('#ErrorMessage').css('display', "none");
            }
            else {
                if (MessageFieldName == "User") {
                    $("#ErrorMessage").html(_UsernameNotAvailable);
                    $('#ErrorMessage').css('display', "");
                    $("#SuccessMessage").html("");
                    $('#SuccessMessage').css('display', "none");
                }
                else {
                    $("#ErrorMessage").html(_AlreadyExists.replace("##FieldName##", MessageFieldName));
                    $('#ErrorMessage').css('display', "");
                    $("#SuccessMessage").html("");
                    $('#SuccessMessage').css('display', "none");
                }
                return false;
            }
        });
    }
}
function BindCity() {
    var stateID = $("#StateID").val();
    if (parseInt(stateID) > 0) {
        var data = { stateID: stateID };
        $.post(SiteBaseUrl + "User/GetCityNameList", data, function (lstData) {
            populateDropdown($("#CityID"), lstData);
        }, "json");
    }
    else {
        $("#CityID").html("- Select City - ");
    }
}