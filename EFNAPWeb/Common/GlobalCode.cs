﻿using EFNAPWeb.Helpers;
using EFNAPWeb.Models;
using EFNAPWeb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Xml;
using System.Web.Mvc;
using System.Web.UI;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Configuration;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace EFNAPWeb.Common
{
    public class GlobalCode
    {
        public static string strEncryptionKey = "EFNAP";
        public static string activeText = "Active";
        public static string inActiveText = "Inactive";
        public static string YesText = "Yes";
        public static string NoText = "No";
        public static string foreignKeyReference = "The DELETE statement conflicted with the REFERENCE constraint";
        public static string sameTableReference = "The DELETE statement conflicted with the SAME TABLE REFERENCE";
        public static int MaxFileSize = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFileSize"]);
        public static int MaxDocumentSize = 15;
        public static int MaxDescriptionLength = 250;
        public static string FavoriteImagePath = "FavoriteImage";

        // Grid Paging Variables
        public static byte pageSize = 10;
        public static byte buttonCount = 5;
        public static string[] PageRecordSize = new[] { "5", "10", "20", "50", "100", "200", "500" };
        public static int[] ExemptedMenuItemID = new[]{2,8,9,13,14,18,19,21};

        public static string strNotAvailable = "N/A";

        //Task Type
        public static string TaskType_Single = "Single";
        public static string TaskType_Recurrence = "Recurrence";

        public static string strJPG = "jpg";
        public static string strJPEG = "jpeg";
        public static string strBMP = "bmp";
        public static string strGIF = "gif";
        public static string strPng = "png";
        public static string strDoc = "doc";
        public static string strPdf = "pdf";
        public static string strTxt = "txt";

        public static string strSignature = "signature";
        public static string strSignatureExtension = ".jpg";

        public static string strHttp = "http://{0}";
        public static string dateFormatGrid = "{0:MM/dd/yyyy}";
        public static string dateTimeFormatGrid = "{0:MM/dd/yyyy hh:mm:ss tt}";
        public static string dateFormat = "MM/dd/yyyy";
        public static string dateTimeFormat = "MM/dd/yyyy hh:mm:ss tt";
        public static string lastLoginFormat = "yyyy-MM-dd hh:mm:ss";
        public static string timeStampFormat = "yyyyMMddHHmmssfff";
        public static string InfoCulture = "en-US";
        public static string strSearchType = "Search Type";
        public static int DefaultDays = 7;

        public static string[] OfficeDocumentsTypes = new string[] { ".doc", ".docx", ".xls", ".xlsx", ".pdf", ".ppt", ".pptx" };
        public static string[] ImageTypes = new string[] { ".jpg", ".jpeg", ".gif", ".png", ".bmp" };

        public enum Actions
        {
            Index,
            Create,
            Edit,
            Delete,
            Detail,
            Search
        }

        public enum FileExtension
        {
            xls,
            xlsx,
            csv,
            xml,
            doc,
            docx,
            pdf
        }

        public enum UserType
        {
            [Description("Internal Agent")]
            InternalAgent = 1,
            [Description("External Agent")]
            ExternalAgent = 2,
            [Description("Seller")]
            Seller = 3,
            [Description("Buyer")]
            Buyer = 4,
            [Description("Affiliate")]
            Affiliates = 5
        }

        public enum FolderFileExtension
        {
            Folder = 1,
            [Description(".doc,.docx")]
            Doc = 2,
            [Description(".xls,.xlsx")]
            Xls = 3,
            [Description(".pdf")]
            Pdf = 4,
            [Description(".txt")]
            Txt = 5,
            [Description(".ppt,.pptx")]
            Ppt = 6,
            [Description(".jpg,.jpeg")]
            jpg = 7,
            [Description(".png")]
            png = 8,
            [Description(".bmp")]
            bmp = 9,
            [Description(".gif")]
            gif = 10
        }

        public static bool IsValidImageFile(string strFileName)
        {
            bool isValidImage = false;
            string strFileExt = Path.GetExtension(strFileName);
            if (!string.IsNullOrEmpty(strFileName))
            {
                if (strFileExt.Contains(GlobalCode.strJPG.ToLower()) || strFileExt.Contains(GlobalCode.strJPEG.ToLower()) || strFileExt.Contains(GlobalCode.strBMP.ToLower()))
                {
                    isValidImage = true;
                }
                else if (strFileExt.Contains(GlobalCode.strGIF.ToLower()) || strFileExt.Contains(GlobalCode.strPng.ToLower()))
                {
                    isValidImage = true;
                }
            }
            return isValidImage;
        }

        public static string GetEnumDescription(Enum value)
        {
            System.Reflection.FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static List<string> GetFileExtensionList()
        {
            var source = Enum.GetValues(typeof(FolderFileExtension)).Cast<FolderFileExtension>();

            var items = new List<string>();

            var displayAttributeType = typeof(DisplayAttribute);

            foreach (var value in source)
            {
                if (!value.Equals(FolderFileExtension.Folder))
                {
                    FieldInfo field = value.GetType().GetField(value.ToString());

                    DescriptionAttribute attrs = (DescriptionAttribute)field.
                                  GetCustomAttributes(typeof(DescriptionAttribute), false).First();

                    items.AddRange(attrs.Description.Split(','));
                }
            }

            return items;
        }

        public static DateTime Get_US_EST_DateTime(DateTime ISTDate)
        {
            TimeZoneInfo SourceTimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            TimeZoneInfo DestinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime DT = DateTime.SpecifyKind(ISTDate, DateTimeKind.Unspecified);
            DateTime ConvertedDt = TimeZoneInfo.ConvertTime(DT, SourceTimeZone, DestinationTimeZone);

            return ConvertedDt;
        }

        public static DataTable ReadExcelData(GlobalCode.FileExtension fileExtension, string filePath, string fileName)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                DataTable dtData = new DataTable();

                if (fileExtension == GlobalCode.FileExtension.xls || fileExtension == GlobalCode.FileExtension.xlsx)
                {
                    try
                    {
                        DataTable dtExcelData = new DataTable();
                        string connectionString = System.Configuration.ConfigurationManager.AppSettings["ExcelConnection"];
                        connectionString = connectionString.Replace("strFileName", filePath + fileName);
                        using (System.Data.OleDb.OleDbConnection connection = new System.Data.OleDb.OleDbConnection(connectionString))
                        {
                            connection.Open();
                            DataTable dtsheetname = connection.GetSchema("Tables");
                            string sheetName = "";
                            if (dtsheetname.Rows.Count > 0)
                            {
                                sheetName = Convert.ToString(dtsheetname.Rows[0]["TABLE_NAME"]);
                            }
                            string strSQL = "SELECT * FROM [" + sheetName + "]";
                            System.Data.OleDb.OleDbCommand dbCommand = new System.Data.OleDb.OleDbCommand(strSQL, connection);
                            System.Data.OleDb.OleDbDataAdapter dataAdapter = new System.Data.OleDb.OleDbDataAdapter(dbCommand);
                            dataAdapter.Fill(dtExcelData);

                        }
                        return dtExcelData;
                    }
                    catch (Exception _Exception)
                    {
                        throw _Exception;
                    }
                }

                return dtData;
            }
            else
                return null;
        }

        public static GlobalCode.FileExtension GetFileExtension(string fileName)
        {
            return (GlobalCode.FileExtension)Enum.Parse(typeof(GlobalCode.FileExtension), System.IO.Path.GetExtension(fileName).Replace(".", string.Empty), true);
        }

        public static string RemoveSpecialCharacters(string inputString)
        {
            string replaceString = System.Text.RegularExpressions.Regex.Replace(inputString, "[^a-zA-Z0-9_]+", string.Empty);
            return replaceString;
        }

        public static string RemoveMaskingCharacter(string inputString)
        {
            if (!string.IsNullOrEmpty(inputString))
            {
                string OutString = inputString.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace("_", string.Empty).Replace(" ", string.Empty);
                return OutString;
            }
            else
                return inputString;
        }

        public static bool CheckPhoneNumberLength(string phoneNumber)
        {
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                return (GlobalCode.RemoveMaskingCharacter(phoneNumber.Trim()).Length == 10 ? true : false);
            }
            else
                return false;
        }

        public static string CreateDocumenFolder(string fixPath, long userID, string documentPath)
        {

            string pathUserID = HttpContext.Current.Server.MapPath(fixPath + Convert.ToString(userID));
            if (!Directory.Exists(pathUserID))
            {
                Directory.CreateDirectory(pathUserID);
            }
            string fullPath = HttpContext.Current.Server.MapPath(fixPath + Convert.ToString(userID) + "/" + documentPath);
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
            return HttpContext.Current.Server.MapPath(fixPath + Convert.ToString(userID) + "/" + documentPath + "/");
        }

        public static string RandomPassword()
        {
            int length = 8;
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        #region Get IP Address
        public static string GetIPAddress()
        {
            string clientIp = (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
            return clientIp;
        }
        #endregion

        public static string FormatPhone(string phone)
        {
            Int64 phoneInt;
            if (!string.IsNullOrEmpty(phone))
            {
                if (phone.Length >= 10)
                {
                    if (Int64.TryParse(phone.Substring(0, 10), out phoneInt))
                    {
                        return phoneInt.ToString("(###) ###-####");
                    }
                }
            }
            return phone;
        }

        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static bool ValidateController(string ControllerName)
        {
            ControllerName = string.IsNullOrEmpty(ControllerName) ? string.Empty : ControllerName.ToLower();
            switch (ControllerName)
            {
                case "document":
                    return false;
                default:
                    return true;
            }
        }

        #region Convert Datatable to List of object
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        #endregion

    }

    public class Encryption
    {
        protected static string strKey = GlobalCode.strEncryptionKey;

        public static string Encrypt(string textToBeEncrypted)
        {
            if (textToBeEncrypted == string.Empty || textToBeEncrypted == null)
            {
                return textToBeEncrypted;
            }

            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            string password = GlobalCode.strEncryptionKey;
            byte[] plainText = System.Text.Encoding.Unicode.GetBytes(textToBeEncrypted);
            byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(password, salt);

            //Creates a symmetric encryptor object.
            ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            //Defines a stream that links data streams to cryptographic transformations
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainText, 0, plainText.Length);

            //Writes the final state and clears the buffer
            cryptoStream.FlushFinalBlock();
            byte[] cipherBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string encryptedData = Convert.ToBase64String(cipherBytes);

            return encryptedData;
        }

        // Used for password dercrption
        public static string Decrypt(string textToBeDecrypted)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            string password = GlobalCode.strEncryptionKey;
            string decryptedData;

            try
            {
                byte[] encryptedData = Convert.FromBase64String(textToBeDecrypted.Replace(' ', '+'));
                byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());

                //Making of the key for decryption
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(password, salt);

                //Creates a symmetric Rijndael decryptor object.
                ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));
                MemoryStream memoryStream = new MemoryStream(encryptedData);

                //Defines the cryptographics stream for decryption.THe stream contains decrpted data
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainText = new byte[encryptedData.Length];
                int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                memoryStream.Close();
                cryptoStream.Close();

                //Converting to string
                decryptedData = Encoding.Unicode.GetString(plainText, 0, decryptedCount);
            }
            catch
            {
                decryptedData = textToBeDecrypted;
            }

            return decryptedData;
        }
    }

    public static class ClsString
    {
        public static string RenderViewToString(this Controller controller, string viewName, string PagePath, object model, bool isPartial)
        {
            controller.ViewData.Model = model;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    RazorView control = new RazorView(controller.ControllerContext, PagePath, null, false, null);
                    controller.ViewData.Model = model;
                    HtmlTextWriter writer = new HtmlTextWriter(new System.IO.StringWriter());
                    control.Render(new ViewContext(controller.ControllerContext, control, controller.ViewData, controller.TempData, writer), writer);
                    string value = ((StringWriter)writer.InnerWriter).ToString();
                    return value;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}