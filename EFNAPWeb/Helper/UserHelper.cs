﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EFNAPWeb.Models;
using System.Data.Entity.Validation;
using EFNAPWeb.Common;
using System.Globalization;
using EFNAP.Repositories.EntityFramework;
using EFNAPWeb.Helpers;
using EFNAP.Domain;

namespace EFNAP.Helpers
{
    public class UserHelper
    {
        #region Get User View Data
        /// <summary>
        /// Get User list based on filter parameter
        /// </summary>
        /// <returns>List of ValidateUser</returns>
        public static List<ValidateUser> GetUserList()
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                List<ValidateUser> userData = (from p in context.User
                                               join r in context.Role on p.RoleID equals r.RoleID
                                               orderby p.Username
                                               select new ValidateUser
                                               {
                                                   UserID = p.UserID,
                                                   Username = p.Username,
                                                   FirstName = p.FirstName,
                                                   LastName = p.LastName,
                                                   Email = p.Email,
                                                   RoleID = p.RoleID,
                                                   Telephone = p.Phone,
                                                   Role = r.RoleName,
                                                   IsAdminUser = p.IsAdminUser,
                                                   Active = p.IsActive == true ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                   IsCurrentAdminUser = CurrentAdminSession.UserID == p.UserID ? true : false
                                               }).ToList();
                return userData;
            }
        }

        #endregion

        #region Get user data for grid
        /// <summary>
        /// Get User list for view
        /// </summary>
        /// <returns>IQueryable of ValidateUser</returns>
        public static IQueryable<ValidateUser> GetUserViewPageList(EFNAPContext ctx)
        {
            var userData = (from p in ctx.User
                            join r in ctx.Role on p.RoleID equals r.RoleID
                            join s in ctx.State on p.StateID equals s.StateID
                            join ct in ctx.City on p.CityID equals ct.CityID
                            join c in ctx.Country on p.CountryID equals c.CountryID
                            orderby p.Username
                            select new ValidateUser
                            {
                                UserID = p.UserID,
                                Username = p.Username,
                                FirstName = p.FirstName,
                                LastName = p.LastName,
                                RoleID = p.RoleID,
                                Role = r.RoleName,
                                IsAdminUser = p.IsAdminUser,
                                State = s.StateName,
                                City = ct.CityName,
                                Country = c.CountryName,
                                IsAdminRole = CurrentAdminSession.RoleID == 1 ? true : false,
                                Active = p.IsActive == true ? GlobalCode.activeText : GlobalCode.inActiveText,
                            });
            return userData;
        }
        #endregion

        #region Get User Count
        /// <summary>
        /// Get User Count
        /// </summary>
        /// <returns>int</returns>
        public static int GetUserCount(string userName, string email, int userID)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                return (from p in context.User
                        where
                        (string.IsNullOrEmpty(userName) || p.Username == userName) && (string.IsNullOrEmpty(email) || p.Email == email)
                        && (userID == 0 || p.UserID != userID)
                        select p.UserID).Count();
            }
        }
        #endregion

        #region Is User Exists By Name or Email
        /// <summary>
        /// check if user exists by name or email
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsUserExistsByNameOrEmail(string fieldList, string valueList, int strAddOrEditID)
        {
            bool IsExists = false;
            string strUserName = string.Empty;
            if (!string.IsNullOrEmpty(fieldList))
            {
                if (strAddOrEditID == -1 && strAddOrEditID != 0)
                {
                    if (fieldList == "Username")
                        strUserName = UserHelper.GetUserCount(valueList.Trim(), string.Empty, 0) == 0 ? null : "1";
                    else
                        strUserName = UserHelper.GetUserCount(string.Empty, valueList.Trim(), 0) == 0 ? null : "1";
                }
                else if (strAddOrEditID != 0)
                {
                    if (fieldList == "Username")
                        strUserName = UserHelper.GetUserCount(valueList.Trim(), string.Empty, strAddOrEditID) == 0 ? null : "1";
                    else
                        strUserName = UserHelper.GetUserCount(string.Empty, valueList.Trim(), strAddOrEditID) == 0 ? null : "1";
                }

                if (!string.IsNullOrEmpty(strUserName))
                {
                    IsExists = true;
                }
            }
            return IsExists;
        }
        #endregion

        #region Add User
        /// <summary>
        /// Add user
        /// </summary>
        /// <returns>void</returns>
        public static void AddUser(ValidateUser validateUser)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                bool IsAdminRole = context.Role.Where(e => e.RoleID == validateUser.RoleID).Select(e => e.IsAdminRole).FirstOrDefault();

                User objUser = new User();
                objUser.FirstName = validateUser.FirstName.Trim();
                objUser.LastName = validateUser.LastName.Trim();
                if (!string.IsNullOrWhiteSpace(validateUser.DateOfBirth))
                    objUser.DateOfBirth = !string.IsNullOrEmpty(validateUser.DateOfBirth) ? DateTime.ParseExact(validateUser.DateOfBirth, GlobalCode.dateFormat, CultureInfo.InvariantCulture) : (DateTime?)null;
                objUser.Phone = validateUser.Telephone;
                objUser.Email = validateUser.Email;
                objUser.RoleID = validateUser.RoleID;
                objUser.CountryID = validateUser.CountryID;
                objUser.StateID = validateUser.StateID;
                objUser.CityID = validateUser.CityID;
                objUser.Username = validateUser.Username.Trim();
                objUser.Password = Encryption.Encrypt(validateUser.Password);
                objUser.IsActive = validateUser.IsActive;
                objUser.IsAdminUser = IsAdminRole;
                objUser.CreatedBy = CurrentAdminSession.UserID;
                objUser.CreatedDate = System.DateTime.Now;
                context.User.Add(objUser);
                context.SaveChanges();
            }
        }
        #endregion

        #region Edit User
        /// <summary>
        /// Edit user
        /// </summary>
        /// <returns>void</returns>
        public static void EditUser(ValidateUser validateUser)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                bool IsAdminRole = context.Role.Where(e => e.RoleID == validateUser.RoleID).Select(e => e.IsAdminRole).FirstOrDefault();

                User objUser = context.User.Find(validateUser.UserID);
                objUser.FirstName = validateUser.FirstName.Trim();
                objUser.LastName = validateUser.LastName.Trim();
                if (!string.IsNullOrWhiteSpace(validateUser.DateOfBirth))
                    objUser.DateOfBirth = !string.IsNullOrEmpty(validateUser.DateOfBirth) ? DateTime.ParseExact(validateUser.DateOfBirth, GlobalCode.dateFormat, CultureInfo.InvariantCulture) : (DateTime?)null;
                objUser.Phone = validateUser.Telephone;
                objUser.Email = validateUser.Email;
                objUser.RoleID = validateUser.RoleID;
                objUser.CountryID = validateUser.CountryID;
                objUser.StateID = validateUser.StateID;
                objUser.CityID = validateUser.CityID;
                objUser.Username = validateUser.Username.Trim();
                objUser.Password = Encryption.Encrypt(validateUser.Password);
                objUser.IsActive = validateUser.IsActive;
                objUser.IsAdminUser = IsAdminRole;
                objUser.ModifiedBy = CurrentAdminSession.UserID;
                objUser.ModifiedDate = System.DateTime.Now;

                context.SaveChanges();
            }
        }
        #endregion

        #region Get Users By IDs
        /// <summary>
        /// Get users by ID
        /// </summary>
        /// <returns>void</returns>
        public static List<User> GetUsersByIds(int[] ids)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                List<User> lstUsers = (from u in context.User
                                       where ids.Contains(u.UserID)
                                       select u).ToList();
                return lstUsers;
            }
        }
        #endregion

        #region Get User BY ID
        /// <summary>
        /// Get single user by UserID
        /// </summary>
        /// <returns>void</returns>
        public static ValidateUser GetUserByID(int userID)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                ValidateUser validateUser = (from u in context.User.AsEnumerable()
                                             join r in context.Role on u.RoleID equals r.RoleID
                                             join s in context.State on u.StateID equals s.StateID
                                             join ct in context.City on u.CityID equals ct.CityID
                                             join c in context.Country on u.CountryID equals c.CountryID
                                             where u.UserID == userID
                                             select new ValidateUser
                                             {
                                                 UserID = u.UserID,
                                                 Username = u.Username,
                                                 Password = Encryption.Decrypt(u.Password),
                                                 ConfirmPassword = Encryption.Decrypt(u.Password),
                                                 FirstName = u.FirstName,
                                                 LastName = u.LastName,
                                                 CountryID = u.CountryID,
                                                 StateID = u.StateID.Value,
                                                 CityID = u.CityID.Value,
                                                 State = s.StateName,
                                                 City = ct.CityName,
                                                 Country = c.CountryName,
                                                 
                                                 dtDateOfBirth = u.DateOfBirth,
                                                 Telephone = u.Phone,
                                                 Email = u.Email,
                                                 IsActive = u.IsActive,
                                                 RoleID = u.RoleID,
                                                 Role = r.RoleName,
                                                 IsAdminUser = u.IsAdminUser,
                                                 Active = u.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                             }).FirstOrDefault();
                return validateUser;
            }
        }
        #endregion

        #region Delete User
        /// <summary>
        /// Delete multiple users
        /// </summary>
        /// <returns>void</returns>
        public static void DeleteUsers(int[] ids)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                var objUser = context.User.Where(u => ids.Contains(u.UserID)).ToList();
                foreach (var item in objUser)
                {
                    context.User.Remove(item);
                }
                context.SaveChanges();
            }
        }
        #endregion

        #region Get User List
        /// <summary>
        /// Get User List
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns>Returns user data</returns>
        public static List<ValidateDropDownBind> GetUserSelectList()
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                List<ValidateDropDownBind> objValidateUserList = (from p in context.User
                                                                  where p.IsActive == true && p.UserID == CurrentAdminSession.UserID
                                                                  select new ValidateDropDownBind
                                                                  {
                                                                      value = p.UserID,
                                                                      name = p.FirstName + " " + p.LastName
                                                                  }).ToList();
                return objValidateUserList;
            }
        }
        #endregion

        #region Get Main Menu List
        /// <summary>
        /// Get Main Menu List
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns>Returns user data</returns>
        public static IList<vwRolePrivileges> GetMainMenuList(int RoleID, List<int?> ParentList)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                IList<vwRolePrivileges> objMainMenu = context.vwRolePrivileges.Where(p => p.ParentID == 0 && p.RoleID == RoleID
                    && p.IsCompanyAdminType == false && ParentList.Contains(p.MenuItemID)).OrderBy(p => p.SortOrder).ToList();
                return objMainMenu;
            }
        }
        #endregion

        #region Get Menu List
        /// <summary>
        /// Get Main Menu List
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns>Returns user data</returns>
        public static IList<vwRolePrivileges> GetMenuList(int RoleID)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                IList<vwRolePrivileges> objMenuList = context.vwRolePrivileges.Where(p => p.RoleID == RoleID && p.View == true && p.ParentID != 0 && p.IsCompanyAdminType == false
                                                    && p.IsActive == 1).OrderBy(p => p.OrderID).ThenBy(p => p.ParentID).ThenBy(p => p.SortOrder).ThenBy(p => p.MenuItem).ToList();
                return objMenuList;
            }
        }
        #endregion

        #region Get User By User Name
        /// <summary>
        /// Get user data by user name
        /// </summary>
        /// <param name="strUserName"></param>
        /// <returns>Returns user data</returns>
        public static ValidateUser GetUserByUserName(string strUserName)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                var userDetail = (from p in context.User
                                  join r in context.Role on p.RoleID equals r.RoleID
                                  where p.Username == strUserName && p.IsActive
                                  select new ValidateUser
                                  {
                                      UserID = p.UserID,
                                      FirstName = p.FirstName,
                                      LastName = p.LastName,
                                      Username = p.Username,
                                      RoleID = p.RoleID,
                                      Password = p.Password,
                                      Telephone = p.Phone,
                                      Email = p.Email,
                                      IsAdminRole = r.IsAdminRole,
                                  }).FirstOrDefault();
                return userDetail;
            }
        }
        #endregion


        #region Is User Exists
        public static bool IsUsernameExists(int UserID, string UserName, EFNAPContext ctx)
        {
            bool result = (from p in ctx.User
                           where (UserID == 0 || p.UserID != UserID) && (string.Compare(p.Username.Trim(), UserName, true) == 0)
                           select p.UserID).Any();
            return result;
        }
        #endregion

        #region Is User Email Exists
        public static bool IsUserEmailExists(int UserID, string Email, EFNAPContext ctx)
        {
            bool result = (from p in ctx.User
                           where (UserID == 0 || p.UserID != UserID) && (string.Compare(p.Email.Trim(), Email, true) == 0)
                           select p.UserID).Any();
            return result;
        }
        #endregion

        #region Get All Active User List of Same Company Except Current User
        public static List<ValidateUser> GetAllActiveUserList(int CurrentUserID)
        {
            using (EFNAPContext _entities = new EFNAPContext())
            {
                List<ValidateUser> objValidateUserList = _entities.User.Where(e => e.UserID != CurrentUserID && e.IsActive == true).Select(e => new ValidateUser
                {
                    UserID = e.UserID,
                    Username = e.Username,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    Email = e.Email,
                    Telephone = e.Phone
                }).ToList();
                return objValidateUserList;
            }
        }
        #endregion

        #region Get User Email
        /// <summary>
        /// Get user email
        /// </summary>
        /// <returns>void</returns>
        public static ValidateUser GetUserEmailByID(int userID)
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                ValidateUser validateUser = (from u in context.User
                                             where u.UserID == userID
                                             select new ValidateUser
                                             {
                                                 UserID = u.UserID,
                                                 Email = u.Email,
                                                 FirstName = u.FirstName,
                                                 LastName = u.LastName,
                                             }).FirstOrDefault();
                return validateUser;
            }
        }
        #endregion

        public static bool GetFavoriteAddPermission()
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                var permission = context.vwRolePrivileges.Where(p => p.RoleID == CurrentAdminSession.RoleID && p.MenuItemController.Trim() == "Favorite" && p.IsActive == 1).FirstOrDefault();
                if (permission != null)
                {
                    return permission.Add != null ? permission.Add.Value : false;
                }
                return false;
            }
        }

        public static bool GetTaskAddPermission()
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                var permission = context.vwRolePrivileges.Where(p => p.RoleID == CurrentAdminSession.RoleID && p.MenuItemController.Trim() == "Task" && p.IsActive == 1).FirstOrDefault();
                if (permission != null)
                {
                    return permission.Add != null ? permission.Add.Value : false;
                }
                return false;
            }
        }

        public static bool GetDocumentAddPermission()
        {
            using (EFNAPContext context = new EFNAPContext())
            {
                var permission = context.vwRolePrivileges.Where(p => p.RoleID == CurrentAdminSession.RoleID && p.MenuItemController.Trim() == "Document" && p.IsActive == 1).FirstOrDefault();
                if (permission != null)
                {
                    return permission.Add != null ? permission.Add.Value : false;
                }
                return false;
            }
        }
    }
}