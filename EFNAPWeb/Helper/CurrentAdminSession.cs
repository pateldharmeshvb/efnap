﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace EFNAPWeb.Helpers
{
    public class CurrentAdminSession
    {
        public static CurrentAdminUser User
        {
            get
            {
                CurrentAdminUser User = (CurrentAdminUser)HttpContext.Current.Session["CurrentAdminUser"];
                return User;
            }
            set
            {
                HttpContext.Current.Session["CurrentAdminUser"] = value;
            }
        }

        public static CurrentAdminPermission Permission
        {
            get
            {
                CurrentAdminPermission Permission = (CurrentAdminPermission)HttpContext.Current.Session["CurrentAdminPermission"];
                return Permission;
            }
            set
            {
                HttpContext.Current.Session["CurrentAdminPermission"] = value;
            }
        }

        public static LayoutPermission LayoutPermission
        {
            get
            {
                LayoutPermission Permission = (LayoutPermission)HttpContext.Current.Session["LayoutPermission"];
                return Permission;
            }
            set
            {
                HttpContext.Current.Session["LayoutPermission"] = value;
            }
        }       
        
        public static int UserID
        {
            get
            {
                if (User != null)
                    return User.UserID;
                else
                    return -1;
            }
        }

        public static int RoleID
        {
            get
            {
                if (User != null)
                    return User.RoleID;
                else
                    return -1;
            }
        }

        public static bool IsAdminRole
        {
            get
            {
                if (User != null)
                    return User.IsAdminRole;
                else
                    return false;
            }
        }

        public static int CompanyID
        {
            get
            {
                if (User != null)
                    return User.CompanyID;
                else
                    return -1;
            }
        }
        
        public static string FirstName
        {
            get
            {
                if (User != null)
                    return User.FirstName;
                else
                    return string.Empty;
            }
        }

        public static string LastName
        {
            get
            {
                if (User != null)
                    return User.LastName;
                else
                    return string.Empty;
            }
        }

        public static string UserName
        {
            get
            {
                if (User != null)
                    return User.UserName;
                else
                    return string.Empty;
            }
        }

        public static string DepartmentName
        {
            get
            {
                if (User != null)
                    return User.DepartmentName;
                else
                    return string.Empty;
            }
        }

        public static string Email
        {
            get
            {
                if (User != null)
                    return User.Email;
                else
                    return string.Empty;
            }
        }

        public static string ReportingPerson
        {
            get
            {
                if (User != null)
                    return User.ReportingPerson;
                else
                    return string.Empty;
            }
        }

        public static DateTime LastLoginDate
        {
            get
            {
                if (User != null)
                    return User.LastLoginDate;
                else
                    return DateTime.Now;
            }
        }

        public static Guid? RootPathKey
        {
            get
            {
                if (User != null)
                    return User.RootPathKey;
                else
                    return (Guid?)null;
            }
        }

        public static bool HasViewPermission
        {
            get
            {
                if (User != null)
                    return Permission.HasViewPermission;
                else
                    return false;
            }
        }

        public static bool HasAddPermission
        {
            get
            {
                if (User != null)
                    return Permission.HasAddPermission;
                else
                    return false;
            }
        }

        public static bool HasFavoriteAddPermission
        {
            get
            {
                if (LayoutPermission != null)
                    return LayoutPermission.HasFavoriteAddPermission;
                else
                    return false;
            }
        }
        
        public static bool HasEditPermission
        {
            get
            {
                if (User != null)
                    return Permission.HasEditPermission;
                else
                    return false;
            }
        }

        public static bool HasDeletePermission
        {
            get
            {
                if (User != null)
                    return Permission.HasDeletePermission;
                else
                    return false;
            }
        }

        public static bool HasDetailPermission
        {
            get
            {
                if (User != null)
                    return Permission.HasDetailPermission;
                else
                    return false;
            }
        }
        public static int CurrentUICulture
        {
            get
            {

                if (Thread.CurrentThread.CurrentUICulture.Name == "gu-IN")
                    return 1;
                else
                    return 0;
            }
            set
            {
                //
                // Set the thread's CurrentUICulture.
                //
                if (value == 1)
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("gu-IN");
                else
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
                //
                // Set the thread's CurrentCulture the same as CurrentUICulture.
                //
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            }
        }
    }

    public class CurrentAdminUser
    {
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public int CompanyID { get; set; }
        
        public string DepartmentName { get; set; }
        public string Email { get; set; }
        public Guid? RootPathKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public bool IsAdminRole { get; set; }
        public string ReportingPerson { get; set; }
        public DateTime LastLoginDate { get; set; }
    }

    public class CurrentAdminPermission
    {
        public bool HasViewPermission { get; set; }
        public bool HasAddPermission { get; set; }
        public bool HasEditPermission { get; set; }
        public bool HasDeletePermission { get; set; }
        public bool HasDetailPermission { get; set; }       
    }

    public class LayoutPermission
    {
        public bool HasFavoriteAddPermission { get; set; }
        public bool HasTaskAddPermission { get; set; }
        public bool HasDocumentAddPermission { get; set; }
    }
}
