﻿using EFNAPWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EFNAPWeb.Helpers
{
    public class ValidateAdminLogin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (CurrentAdminSession.UserID < 0)
            {
                filterContext.HttpContext.Session.RemoveAll();
                filterContext.HttpContext.Session.Clear();
                filterContext.HttpContext.Session.Abandon();
                try
                {
                    filterContext.Result = new RedirectResult("~/Home/Index?Msg=UnAuthorised&ReturnPath=" + filterContext.ActionDescriptor.ControllerDescriptor.ControllerName + "/" + filterContext.ActionDescriptor.ActionName);
                }
                catch(Exception)
                {
                    filterContext.Result = new RedirectResult("~/Home/Index?Msg=UnAuthorised");
                }                
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class ValidateAdminPermission : ActionFilterAttribute
    {
        GlobalCode.Actions act;

        public ValidateAdminPermission(GlobalCode.Actions Action)
        {
            act = Action;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isAuthorize = true;
            if (act == GlobalCode.Actions.Index && !CurrentAdminSession.HasViewPermission)
            {
                isAuthorize = false;
            }

            if (act == GlobalCode.Actions.Create && !CurrentAdminSession.HasAddPermission)
            {
                isAuthorize = false;
            }

            if (act == GlobalCode.Actions.Edit && !CurrentAdminSession.HasEditPermission)
            {
                isAuthorize = false;
            }

            if (act == GlobalCode.Actions.Delete && !CurrentAdminSession.HasDeletePermission)
            {
                isAuthorize = false;
            }

            if (act == GlobalCode.Actions.Detail && !CurrentAdminSession.HasDetailPermission)
            {
                isAuthorize = false;
            }

            if (!isAuthorize)
            {
                filterContext.Result = new RedirectResult("~/Home/Index?Msg=UnAuthorised");
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }
    }
}