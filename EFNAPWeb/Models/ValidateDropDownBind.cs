﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;

namespace EFNAPWeb.Models
{
    public class ValidateDropDownBind
    {
        public int value { get; set; }
        public string name { get; set; }
    }

    public class Select2Result
    {
        public int id { get; set; }
        public string text { get; set; }
    }
}