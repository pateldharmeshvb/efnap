﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Management;
using System.Text;
using System.Web.Routing;
using System.Configuration;
using System.Globalization;
using System.Security.Cryptography;
using EFNAPWeb.Helpers;

namespace EFNAPWeb.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        #region Index
        /// <summary>
        /// Login page get method
        /// </summary>
        /// <returns>Returns login page view</returns>
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region Index Method
        /// <summary>
        /// Login page post method for redirecting validate user to home page 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If user validated successfully then redirect to home page otherwise error return message on login view</returns>
        [HandleError]
        [HttpPost]
        public ActionResult Index(int? id)
        {
            if (!string.IsNullOrEmpty(Request.Form["UserName"]) && !string.IsNullOrEmpty(Request.Form["UserPassword"]))
            {
                using (EFNAP.Repositories.EntityFramework.EFNAPContext db = new EFNAP.Repositories.EntityFramework.EFNAPContext())
                {
                    string strUserName = Convert.ToString(Request.Form["UserName"]);
                    string strUserPassword = Convert.ToString(Request.Form["UserPassword"]);
                    var userDetail = (from u in db.User where u.Username.Trim().Equals(strUserName.Trim()) select u).FirstOrDefault();
                    if (userDetail != null)
                    {
                        if (strUserPassword == Encryption.Decrypt(userDetail.Password))
                        {
                            CurrentAdminUser User = new CurrentAdminUser();
                            User.UserID = userDetail.UserID;
                            User.RoleID = userDetail.RoleID;
                            User.FirstName = userDetail.FirstName;
                            User.LastName = userDetail.LastName;
                            User.UserName = userDetail.Username;
                            User.Email = userDetail.Email;
                            User.IsAdminRole = userDetail.IsAdminUser;
                            CurrentAdminSession.User = User;
                            Session["UserID"] = userDetail.UserID;
                            IList<EFNAP.Domain.vwRolePrivileges> menuItems = null;
                            IList<EFNAP.Domain.vwRolePrivileges> mainItems = null;
                            EFNAP.Services.RolePrivilegesManagement objRpm = new EFNAP.Services.RolePrivilegesManagement();
                            EFNAP.Services.UserManagement objUM = new EFNAP.Services.UserManagement();
                            var ParentList = objRpm.GetParentIDsByRole(userDetail.RoleID);
                            mainItems = objUM.GetMainMenuList(userDetail.RoleID, ParentList);
                            menuItems = objUM.GetMenuList(userDetail.RoleID);
                            Session["MainItems"] = mainItems;
                            Session["MenuItems"] = menuItems;

                            return RedirectToAction("Home", "Home");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Incorrect Username or Password");
                            return View();
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Incorrect Username or Password");
                        return View();
                    }
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Some field is required.");
                return View();
            }
        }
        #endregion

        #region Home
        /// <summary>
        /// Home get method
        /// </summary>
        /// <returns>Returns home view</returns>
        public ActionResult Home()
        {
            return View();
        }
        #endregion

        #region Unauthorised
        /// <summary>
        /// UnAuthorised get method 
        /// </summary>
        /// <returns>Returns unauthorised view</returns>
        [HttpGet]
        [ValidateAdminLogin]
        public ActionResult UnAuthorised()
        {
            return View();
        }

        #endregion

        #region LogOut
        /// <summary>
        /// Logout get method for clearing session data and redirecting to login page
        /// </summary>
        /// <returns>Redirects to login page</returns>
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

            return RedirectToAction("Index", "Home", new { Msg = "Logout" });
        }
        #endregion

    }

    public class Encryption
    {
        protected static string strKey = "EFNAPWeb";

        public static string Encrypt(string textToBeEncrypted)
        {
            if (textToBeEncrypted == string.Empty || textToBeEncrypted == null)
            {
                return textToBeEncrypted;
            }

            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            string password = "EFNAPWeb";
            byte[] plainText = System.Text.Encoding.Unicode.GetBytes(textToBeEncrypted);
            byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(password, salt);

            //Creates a symmetric encryptor object.
            ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            //Defines a stream that links data streams to cryptographic transformations
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainText, 0, plainText.Length);

            //Writes the final state and clears the buffer
            cryptoStream.FlushFinalBlock();
            byte[] cipherBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string encryptedData = Convert.ToBase64String(cipherBytes);

            return encryptedData;
        }

        // Used for password dercrption
        public static string Decrypt(string textToBeDecrypted)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            string password = "EFNAPWeb";
            string decryptedData;

            try
            {
                byte[] encryptedData = Convert.FromBase64String(textToBeDecrypted.Replace(' ', '+'));
                byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());

                //Making of the key for decryption
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(password, salt);

                //Creates a symmetric Rijndael decryptor object.
                ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));
                MemoryStream memoryStream = new MemoryStream(encryptedData);

                //Defines the cryptographics stream for decryption.THe stream contains decrpted data
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainText = new byte[encryptedData.Length];
                int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                memoryStream.Close();
                cryptoStream.Close();

                //Converting to string
                decryptedData = Encoding.Unicode.GetString(plainText, 0, decryptedCount);
            }
            catch
            {
                decryptedData = textToBeDecrypted;
            }

            return decryptedData;
        }
    }
}