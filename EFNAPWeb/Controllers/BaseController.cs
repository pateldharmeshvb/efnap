﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EFNAPWeb.Helpers;

namespace EFNAPWeb.Controllers
{
    public class BaseController : AsyncController
    {
        #region Getting User Rights & Page Information
        /// <summary>
        /// Method for getting user rights and page information (title, keyword, description)
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
            }
            ModulePrivileges(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName);
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Method for getting user rights of login user by controller name and set it in session object 
        /// </summary>
        /// <param name="controller"></param>
        private void ModulePrivileges(string controller)
        {
            EFNAP.Domain.vwRolePrivileges rolePrivileges = new EFNAP.Domain.vwRolePrivileges();
            using (EFNAP.Repositories.EntityFramework.EFNAPContext _Entities = new EFNAP.Repositories.EntityFramework.EFNAPContext())
            {
                if (controller == "Recipient")
                {
                    IList<EFNAP.Domain.vwRolePrivileges> lst = (IList<EFNAP.Domain.vwRolePrivileges>)Session["MenuItems"];
                    rolePrivileges = lst.Where(p => p.RoleID == CurrentAdminSession.RoleID && p.MenuItemController.Trim() == controller && p.IsActive == 1).FirstOrDefault();
                }
                else
                {
                    rolePrivileges = _Entities.vwRolePrivileges.Where(p => p.RoleID == CurrentAdminSession.RoleID && p.MenuItemController.Trim() == controller && p.IsActive == 1).FirstOrDefault();
                }

                if (rolePrivileges != null)
                {
                    CurrentAdminPermission Permission = new CurrentAdminPermission();
                    Permission.HasViewPermission = rolePrivileges.View.Value;
                    Permission.HasAddPermission = rolePrivileges.Add.Value;
                    Permission.HasEditPermission = rolePrivileges.Edit.Value;
                    Permission.HasDeletePermission = rolePrivileges.Delete.Value;
                    Permission.HasDetailPermission = rolePrivileges.Detail.Value;
                    CurrentAdminSession.Permission = Permission;
                }
                else
                {
                    CurrentAdminPermission Permission = new CurrentAdminPermission();
                    Permission.HasViewPermission = false;
                    Permission.HasAddPermission = false;
                    Permission.HasEditPermission = false;
                    Permission.HasDeletePermission = false;
                    Permission.HasDetailPermission = false;
                    CurrentAdminSession.Permission = Permission;
                }
            }
        }

        #endregion
    }
}