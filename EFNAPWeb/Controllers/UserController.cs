﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EFNAPWeb.Models;
using EFNAPWeb.Helpers;
using System.Threading.Tasks;
using System.Threading;
using System.Data.Entity;
using EFNAPWeb.Common;
using System.Collections;
using System.Globalization;
using EFNAP.Repositories.EntityFramework;
using EFNAP.Helpers;
using EFNAPWeb.Helper;

namespace EFNAPWeb.Controllers
{
    [ValidateAdminLogin]
    public class UserController : BaseController
    {
        #region Index
        /// <summary>
        /// User List – This method is used to display list of user.
        /// </summary>
        /// <returns>Returns user index view</returns>
        [HandleError]
        [HttpGet]
        [ValidateAdminPermission(GlobalCode.Actions.Index)]
        public ActionResult Index()
        {
            var viewModel = new ValidateUser
            {
                //IListUser = UserHelper.GetUserList(),
                ModuleName = new ValidateCommonMessages { ModuleName = "User" },
            };
            return View(viewModel);
        }

        #endregion

        #region Create
        /// <summary>
        /// Create User – This method is used to view create user page.
        /// </summary>
        /// <returns>Returns user create view</returns>
        [HandleError]
        [HttpGet]
        [ValidateAdminPermission(GlobalCode.Actions.Create)]
        public ActionResult Create()
        {
            ValidateUser objValidateUser = new ValidateUser();
            objValidateUser = BindAllDropDown(objValidateUser);
            objValidateUser.IsActive = true;
            return View(objValidateUser);
        }

        /// <summary>
        /// Create User – This method is used to create user.
        /// </summary>
        /// <param name="objValidateUser"></param>
        /// <returns>If user data saved succesfully then returns success message other wise returns user view with error message</returns>
        [HandleError]
        [HttpPost]
        public ActionResult Create([Bind(Exclude = "UserID")] ValidateUser objValidateUser)
        {
            if (ModelState.IsValid)
            {
                bool duplicateStatus = UserHelper.IsUserExistsByNameOrEmail("Username", objValidateUser.Username.Trim(), -1);
                bool emailDuplicateStatus = UserHelper.IsUserExistsByNameOrEmail("Email", objValidateUser.Email.Trim(), -1);
                DateTime? dtDOB = !string.IsNullOrEmpty(objValidateUser.DateOfBirth) ? DateTime.ParseExact(objValidateUser.DateOfBirth, GlobalCode.dateFormat, CultureInfo.InvariantCulture) : (DateTime?)null;
                if (duplicateStatus)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Username "));
                    objValidateUser = BindAllDropDown(objValidateUser);
                    return View(objValidateUser);
                }
                else if (emailDuplicateStatus)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Email "));
                    objValidateUser = BindAllDropDown(objValidateUser);
                    return View(objValidateUser);
                }
                else if (!string.IsNullOrEmpty(objValidateUser.DateOfBirth) && dtDOB.Value.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.DateNotGreaterthanCurrent, "Date Of Birth"));
                    objValidateUser = BindAllDropDown(objValidateUser);
                    return View(objValidateUser);
                }
                UserHelper.AddUser(objValidateUser);
                return RedirectToAction("Index", "User", new { Msg = "added" });
            }
            else
            {
                objValidateUser = BindAllDropDown(objValidateUser);
                return View(objValidateUser);
            }
        }

        #endregion

        #region Edit
        /// <summary>
        /// Edit - this method is used to edit user detail
        /// <param name="id"></param>
        /// <returns>Returns user edit view</returns>
        [HandleError]
        [ValidateAdminPermission(GlobalCode.Actions.Edit)]
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index", "User");
            }
            else
            {
                ValidateUser objValidateUser = UserHelper.GetUserByID(Convert.ToInt32(id));
                if (objValidateUser == null)
                {
                    return RedirectToAction("Index", "User", new { Msg = "drop" });
                }
                objValidateUser = BindAllDropDown(objValidateUser);
                objValidateUser.DateOfBirth = objValidateUser.dtDateOfBirth.HasValue ? objValidateUser.dtDateOfBirth.Value.ToString(GlobalCode.dateFormat) : string.Empty;
                objValidateUser.Password = Encryption.Decrypt(objValidateUser.Password);
                objValidateUser.ConfirmPassword = Encryption.Decrypt(objValidateUser.Password);
                return View(objValidateUser);
            }
        }
        /// <summary>
        /// Edit – This method is used to edit user information.
        /// </summary>
        /// <param name="objValidateUser"></param>
        /// <returns>If user data updated succesfully then returns success message other wise returns user view with error message</returns>
        //[HandleError]
        [HttpPost]
        public ActionResult Edit(ValidateUser objValidateUser)
        {
            if (ModelState.IsValid)
            {
                var objUser = UserHelper.GetUserByID(objValidateUser.UserID);
                if (objUser == null)
                {
                    return RedirectToAction("Index", "User", new { Msg = "drop" });
                }
                bool duplicateStatus = UserHelper.IsUserExistsByNameOrEmail("Username", objValidateUser.Username.Trim(), objValidateUser.UserID);
                bool emailDuplicateStatus = UserHelper.IsUserExistsByNameOrEmail("Email", objValidateUser.Email.Trim(), objValidateUser.UserID);
                DateTime? dtDOB = !string.IsNullOrEmpty(objValidateUser.DateOfBirth) ? DateTime.ParseExact(objValidateUser.DateOfBirth, GlobalCode.dateFormat, CultureInfo.InvariantCulture) : (DateTime?)null;
                if (duplicateStatus)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Username "));
                    objValidateUser = BindAllDropDown(objValidateUser);
                    return View(objValidateUser);
                }
                else if (emailDuplicateStatus)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Email "));
                    objValidateUser = BindAllDropDown(objValidateUser);
                    return View(objValidateUser);
                }
                else if (!string.IsNullOrEmpty(objValidateUser.DateOfBirth) && dtDOB.Value.Date > DateTime.Now.Date)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.DateNotGreaterthanCurrent, "Date Of Birth"));
                    objValidateUser = BindAllDropDown(objValidateUser);
                    return View(objValidateUser);
                }
                UserHelper.EditUser(objValidateUser);
                CurrentAdminUser currentUser = CurrentAdminSession.User;
                currentUser.FirstName = objValidateUser.FirstName.Trim();
                currentUser.LastName = objValidateUser.LastName.Trim();
                currentUser.RoleID = objValidateUser.RoleID;
                currentUser.UserName = objValidateUser.Username.Trim();
                CurrentAdminSession.User = currentUser;
                return RedirectToAction("Index", "User", new { Msg = "updated" });
            }
            else
            {
                objValidateUser = BindAllDropDown(objValidateUser);
                return View(objValidateUser);
            }
        }
        #endregion

        #region Detail
        /// <summary>
        /// Detail – This method is used to display user information.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns user detail view</returns>
        [HandleError]
        [HttpGet]
        [ValidateAdminPermission(GlobalCode.Actions.Detail)]
        public ActionResult Detail(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction("Index", "User");
            }
            else
            {
                ValidateUser objValidateUser = UserHelper.GetUserByID(Convert.ToInt32(id));
                if (objValidateUser == null)
                {
                    return RedirectToAction("Index", "User", new { Msg = "drop" });
                }
                else
                {
                    objValidateUser.DateOfBirth = objValidateUser.dtDateOfBirth.HasValue ? objValidateUser.dtDateOfBirth.Value.ToString(GlobalCode.dateFormat) : "";
                    return View(objValidateUser);
                }
            }
        }
        #endregion

        #region Validate Duplicate User
        /// <summary>
        /// Duplicate User – This method is used to check department is already exist or not
        /// </summary>        
        public ActionResult ValidateDuplicateUser(string Username, int? UserID)
        {
            bool result;
            using (EFNAPContext ctx = new EFNAPContext())
            {
                result = UserHelper.IsUsernameExists(Convert.ToInt32(UserID), Username, ctx);
            }
            return Json(!result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Bind All Drop Down
        private ValidateUser BindAllDropDown(ValidateUser objValidateUser)
        {
            using (EFNAPContext ctx = new EFNAPContext())
            {
                objValidateUser.CountryList = GetCountryList();
                objValidateUser.StateList = GetStateList();
                objValidateUser.CityList = GetCityList(objValidateUser.StateID);
                objValidateUser.SelectRole = new SelectList(ctx.Role.Where(p => p.IsActive == true).Select(x => new { name = x.RoleName, value = x.RoleID }).ToList(), "value", "name");
                return objValidateUser;
            }
        }
        #endregion

        #region JSON Method to Get Parent list for DropdownList
        /// <summary>
        /// Get parent list by parent type
        /// </summary>
        /// <returns>Returns list of parent data by parent type in json</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCityNameList(int stateID)
        {
            using (var db = new EFNAPContext())
            {
                var state = db.State.Where(p => p.StateID == stateID).FirstOrDefault().StateCode;
                var stateList = db.City.Where(x => x.StateCode.Equals(state)).Select(p => new { value = p.CityID, name = p.CityName }).ToList();
                return Json(stateList);
            }
        }
        #endregion

        #region Get Enum List
        public SelectList GetCountryList()
        {
            using (var db = new EFNAPContext())
            {
                var countryList = db.Country.Select(p => new { Value = p.CountryID, Text = p.CountryName }).ToList();
                return new SelectList(countryList, "Value", "Text");
            }
        }
        public SelectList GetStateList()
        {
            using (var db = new EFNAPContext())
            {
                var stateList = db.State.Select(p => new { Value = p.StateID, Text = p.StateName }).ToList();
                return new SelectList(stateList, "Value", "Text");
            }
        }
        public SelectList GetCityList(int StateID)
        {
            using (var db = new EFNAPContext())
            {
                if (StateID > 0)
                {
                    var state = db.State.Where(p => p.StateID == StateID).FirstOrDefault().StateCode;
                    var stateList = db.City.Where(x => x.StateCode.Equals(state)).Select(p => new { Value = p.CityID, Text = p.CityName }).ToList();
                    return new SelectList(stateList, "Value", "Text");
                }
                return new SelectList(string.Empty, "Value", "Text");
            }
        }
        #endregion

        #region Delete
        public ActionResult Delete(long id)
        {
            try
            {
                if (id > 0)
                {
                    using (EFNAPContext _entities = new EFNAPContext())
                    {
                        var objUser = _entities.User.Where(p => p.UserID == id).FirstOrDefault();
                        _entities.User.Remove(objUser);
                        _entities.SaveChanges();
                        return Json(new { msg = "deleted" });
                    }
                }
                else
                {
                    return Json(new { msg = "NoSelect" });
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((ex.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference))
                {
                    return Json(new { msg = "inuse" });
                }
                else
                {
                    return Json(new { msg = string.Empty });
                }
            }
        }
        #endregion

        #region Get User List

        [HttpGet]
        public JsonResult GetUsersList(int? page, int? limit, string sortBy, string direction, string firstName = null, string lastName = null, string email = null, string phone = null)
        {
            EFNAPContext _entities = new EFNAPContext();

            int total;
            var records = (from p in _entities.User
                           where (string.IsNullOrEmpty(firstName) || p.FirstName.Contains(firstName))
                            && (string.IsNullOrEmpty(lastName) || p.LastName.Contains(lastName))
                            && (string.IsNullOrEmpty(email) || p.Email.Contains(email))
                            && (string.IsNullOrEmpty(phone) || p.Phone.Contains(phone))
                           orderby p.FirstName
                           select new ValidateUser
                           {
                               UserID = p.UserID,
                               FirstName = p.FirstName,
                               LastName = p.LastName,
                               Email = p.Email,
                               Telephone = p.Phone,
                               Active = p.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText
                           }).AsQueryable();

            total = records.Count();
            if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(direction))
            {
                if (direction.Trim().ToLower() == "asc")
                {
                    records = SortHelper.OrderBy(records, sortBy);
                }
                else
                {
                    records = SortHelper.OrderByDescending(records, sortBy);
                }
            }

            if (page.HasValue && limit.HasValue)
            {
                int start = (page.Value - 1) * limit.Value;
                records = records.Skip(start).Take(limit.Value);
            }

            return Json(new { records, total }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}