﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFNAP.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace EFNAP.Domain
{
    public partial class State
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }
    }
}
