﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFNAP.Domain
{
    public partial class Role
    {
        public Role()
        {
            this.User = new HashSet<User>();
        }

        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public bool IsAdminRole { get; set; }
        public int CompanyID { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
