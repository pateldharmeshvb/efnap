﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFNAP.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace EFNAP.Domain
{
    public partial class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }
}
