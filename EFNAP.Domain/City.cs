﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFNAP.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace EFNAP.Domain
{
    public partial class City
    {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string StateCode { get; set; }
    }
}
