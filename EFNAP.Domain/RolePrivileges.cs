﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFNAP.Domain
{
    public partial class RolePrivileges
    {
        [Key]
        public int PrivilegeID { get; set; }
        public int RoleID { get; set; }
        public Nullable<int> MenuItemID { get; set; }
        public Nullable<bool> View { get; set; }
        public Nullable<bool> Add { get; set; }
        public Nullable<bool> Edit { get; set; }
        public Nullable<bool> Delete { get; set; }
        public Nullable<bool> Detail { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int CompanyID { get; set; }
        public bool IsActive { get; set; }
    }
}
