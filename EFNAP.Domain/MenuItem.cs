﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFNAP.Domain
{
    public partial class MenuItem
    {
        public int MenuItemID { get; set; }
        public string MenuItemName { get; set; }
        public string MenuItemController { get; set; }
        public string MenuItemView { get; set; }
        public Nullable<int> SortOrder { get; set; }
        public Nullable<int> ParentID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> IsCompanyAdminType { get; set; }
    }
}
